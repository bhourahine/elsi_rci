### ELSI RCI tests ###
LIST(APPEND ftest_rci_src
  elsi_rci_test.f90
  elsi_rci_utils/elsi_rci_read_mat.f90
  elsi_rci_utils/elsi_rci_sort.f90
  elsi_rci_utils/elsi_rci_timer.f90
  elsi_rci_utils/elsi_rci_write_mat.f90
  test_rci_ev_real_den.f90
#  test_rci_ev_real_csc.f90
  test_rci_ev_cmplx_den.f90
#  test_rci_ev_cmplx_csc.f90
  )
find_file(MKL_DFTI mkl_dfti.f90 $ENV{MKLROOT}/include/)
if (MKL_DFTI)
  message(STATUS "looking for mkl_dfti.f90 - found")
  message(STATUS "activate the pw tests")
  LIST(APPEND ftest_rci_src 
    test_rci_ev_cmplx_pw.f90
    test_rci_ev_cmplx_pw_unit.f90
    $ENV{MKLROOT}/include/mkl_dfti.f90)
else()
  LIST(APPEND ftest_rci_src 
    test_rci_ev_cmplx_pw_unit_stub.f90)
  message(STATUS "looking for mkl_dfti.f90 - not found")
  message(STATUS "deactivate the pw tests")
endif()

ADD_EXECUTABLE(elsi_rci_test ${ftest_rci_src})
TARGET_LINK_LIBRARIES(elsi_rci_test PRIVATE elsi_rci)
TARGET_LINK_LIBRARIES(elsi_rci_test PRIVATE ${LIBS})
SET_TARGET_PROPERTIES(elsi_rci_test PROPERTIES LINKER_LANGUAGE Fortran)

SET(h_real "${PROJECT_SOURCE_DIR}/test/matrices/H_real.csc")
SET(s_real "${PROJECT_SOURCE_DIR}/test/matrices/S_real.csc")
SET(h_cmplx "${PROJECT_SOURCE_DIR}/test/matrices/H_complex.csc")
SET(s_cmplx "${PROJECT_SOURCE_DIR}/test/matrices/S_complex.csc")
SET(h_pw "${PROJECT_SOURCE_DIR}/test/matrices/Si8.kssolv")

MACRO(test_fortran arg1 arg2 arg3 arg4 arg5 arg6 arg7)
  ADD_TEST(test_fortran_${arg1}
    ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/elsi_rci_test
    ${arg2} ${arg3} ${arg4} ${arg5} ${arg6} ${arg7})
  SET_TESTS_PROPERTIES(test_fortran_${arg1}
    PROPERTIES PASS_REGULAR_EXPRESSION "Passed")
ENDMACRO()

test_fortran(01_davidson_real    e 0 r 1 ${h_real} ${s_real})
test_fortran(02_davidson_cmplx   e 0 c 1 ${h_cmplx} ${s_cmplx})
if (MKL_DFTI)
test_fortran(03_davidson_pw      e 3 c 1 ${h_pw} ${h_pw})
endif ()
test_fortran(04_omm_real         e 0 r 2 ${h_real} ${s_real})
test_fortran(05_omm_cmplx        e 0 c 2 ${h_cmplx} ${s_cmplx})
if (MKL_DFTI)
test_fortran(06_omm_pw           e 3 c 2 ${h_pw} ${h_pw})
endif ()
test_fortran(07_ppcg_real        e 0 r 3 ${h_real} ${s_real})
test_fortran(08_ppcg_cmplx       e 0 c 3 ${h_cmplx} ${s_cmplx})
if (MKL_DFTI)
test_fortran(09_ppcg_pw          e 3 c 3 ${h_pw} ${h_pw})
endif ()
test_fortran(10_chebfilter_real  e 0 r 4 ${h_real} ${s_real})
test_fortran(11_chebfilter_cmplx e 0 c 4 ${h_cmplx} ${s_cmplx})
if (MKL_DFTI)
test_fortran(12_chebfilter_pw    e 3 c 4 ${h_pw} ${h_pw})
endif ()
