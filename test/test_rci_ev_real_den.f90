! Copyright (c) 2015-2019, the ELSI team.
! All rights reserved.
!
! This file is part of ELSI and is distributed under the BSD 3-clause license,
! which may be found in the LICENSE file in the ELSI root directory.

!>
!! This subroutine tests complex iterative eigensolver, BLACS_DENSE format.
!!
subroutine test_rci_ev_real_den(solver,h_file,s_file)

    use ELSI_RCI ! User should use this
    use ELSI_RCI_PRECISION ! Only for test
    use ELSI_RCI_READ_MAT ! Only for test
    use ELSI_RCI_TIMER ! Only for test

    implicit none

    integer(i4), intent(in) :: solver
    character(*), intent(in) :: h_file
    character(*), intent(in) :: s_file

    ! Reference values
    real(r8), parameter :: e_elpa  = -1282.30981862024_r8

    integer(i4) :: n_state = 0
    integer(i4) :: task
    integer(i4) :: ijob
    integer(i4) :: it, iti, itj
    integer(i4) :: lWorktmp, info
    integer(i4) :: lda, ldb, ldc

    integer(i4) :: max_iter = 200
    real(r8) :: tol_iter = 1.e-8_r8
    integer(i4) :: verbose = 1
    real(r8) :: x
    real(r8) :: denm
    real(r8) :: t1
    real(r8) :: t2
    real(r8) :: rnum

    real(r8) :: e_test
    real(r8) :: e_ref
    real(r8) :: e_tol = 1.e-1_r8

    type(rci_handle) :: r_h

    type(rci_instr)      :: iS
    real(r8), allocatable :: resvec(:)

    real(r8), allocatable :: Mattmp(:, :)
    real(r8), allocatable :: Worktmp(:)

    type :: matrix
        real(r8), allocatable :: Mat(:, :)
    end type matrix

    type(matrix), allocatable :: Work(:)

    write (*, '("  ####################################")')
    write (*, '("  ##   ELSI RCI REAL TEST PROGRAMS  ##")')
    write (*, '("  ####################################")')
    write (*, *)
    write (*, '("  This test program performs the following steps:")')
    write (*, '("  1) Reads Hamiltonian and overlap matrices;")')
    if (solver == RCI_SOLVER_DAVIDSON) then
        write (*, '("  2) Computes the eigenfunc with RCI_DAVIDSON.")')
    endif
    if (solver == RCI_SOLVER_OMM) then
        write (*, '("  2) Computes the eigenfunc with RCI_OMM.")')
    endif
    if (solver == RCI_SOLVER_PPCG) then
        write (*, '("  2) Computes the eigenfunc with RCI_PPCG.")')
    endif
    if (solver == RCI_SOLVER_CHEBFILTER) then
        write (*, '("  2) Computes the eigenfunc with RCI_CHEBFILTER.")')
    endif
    write (*, *)

    e_ref = e_elpa

    call rci_init_timer()
    call rci_get_time(t1)

    call rci_read(h_file, s_file, .true.)
    n_state = int(n_electron,i4)

    call rci_get_time(t2)

    write (*, '("  Finished reading H and S matrices")')
    write (*, '("  | Time   :",F10.3,"s")') t2 - t1
    write (*, '("  | nBasis :",I10 )') n_basis
    write (*, '("  | nState :",I10 )') n_state
    write (*, *)

    call rci_get_time(t1)

    call rci_init(r_h, solver, n_basis, n_state, tol_iter, max_iter, &
        verbose)

    ! Set the estimated lower and upper bounds for the spectrum
    r_h%cheb_est_lb = 0.8_r8
    r_h%cheb_est_ub = 100.0_r8
    r_h%omm_est_ub = 100.0_r8

    allocate (Work(100))

    ijob = RCI_INIT_IJOB
    do
        call rci_solve_allocate(r_h, ijob, iS, task)
        select case (task)
        case (RCI_NULL)
        case (RCI_STOP)
            exit
        case (RCI_ALLOCATE)
            allocate (Work(iS%Aidx)%Mat(iS%m, iS%n))
            Work(iS%Aidx)%Mat = 0.0_r8
        case default
        end select
    end do


    ! Initialize wave functions
    do itj = 1, n_state
        do iti = 1, n_basis
            call random_number(rnum)
            Work(1)%Mat(iti, itj) = rnum*1.0d-2/sqrt(real(n_basis, r8))
        end do
    end do

    allocate (resvec(r_h%n_res))
    resvec = 0.0_r8
    ijob = RCI_INIT_IJOB
    do
        call rci_solve(r_h, ijob, iS, task, resvec)

        select case (task)
        case (RCI_NULL)
        case (RCI_STOP)
            write (*, *) 'Maximum iteration number is reached.'
            exit
        case (RCI_CONVERGE)
            exit

        case (RCI_H_MULTI)
            lda = n_basis
            ldb = n_basis
            !print *, 'H_multi: ', iS%m, iS%n
            call dgemm(iS%TrH, 'N', n_basis, iS%n, iS%m, 1.0_r8, &
                       h_real, n_basis, Work(iS%Aidx)%Mat, lda, 0.0_r8, &
                       Work(iS%Bidx)%Mat, ldb)

        case (RCI_S_MULTI)
            lda = n_basis
            ldb = n_basis
            call dgemm(iS%TrS, 'N', n_basis, n_state, n_basis, 1.0_r8, &
                       s_real, n_basis, Work(iS%Aidx)%Mat, lda, 0.0_r8, &
                       Work(iS%Bidx)%Mat, ldb)

        case (RCI_P_MULTI)
            Work(iS%Bidx)%Mat = Work(iS%Aidx)%Mat
!            do itj = 1, iS%n
!                do iti = 1, iS%m
!                    x = h_real(iti, iti) - resvec(itj) * s_real(iti, iti)
!                    denm = 0.5_r8 * (1.0_r8 + x &
!                        + sqrt(1.0_r8 + (x - 1.0_r8) * (x - 1.0_r8)))
!                    Work(iS%Bidx)%Mat(iti, itj) &
!                        = - Work(iS%Aidx)%Mat(iti, itj)/denm
!                end do
!            end do

        case (RCI_GEMM)
            call dgemm(iS%trA, iS%trB, iS%m, iS%n, iS%k, iS%alpha, &
                       Work(iS%Aidx)%Mat, iS%lda, &
                       Work(iS%Bidx)%Mat, iS%ldb, &
                       iS%beta, Work(iS%Cidx)%Mat, iS%ldc)

        case (RCI_AXPY)
            call daxpy(iS%m*iS%n, iS%alpha, Work(iS%Aidx)%Mat, 1, &
                       Work(iS%Bidx)%Mat, 1)

        case (RCI_COPY)
            if (iS%trA == 'N') then
                Work(iS%Bidx)%Mat = Work(iS%Aidx)%Mat
            else
                Work(iS%Bidx)%Mat = transpose(Work(iS%Aidx)%Mat)
            endif

        case (RCI_SUBCOPY)
            Work(iS%Bidx)%Mat(iS%rBoff + 1:iS%rBoff + iS%m, &
                              iS%cBoff + 1:iS%cBoff + iS%n) &
                = Work(iS%Aidx)%Mat(iS%rAoff + 1:iS%rAoff + iS%m, &
                                    iS%cAoff + 1:iS%cAoff + iS%n)

        case (RCI_SCALE)
            Work(iS%Aidx)%Mat = iS%alpha*Work(iS%Aidx)%Mat

        case (RCI_TRACE)
            resvec(1) = 0.0_r8
            do it = 1, iS%n
                resvec(1) = resvec(1) + Work(iS%Aidx)%Mat(it, it)
            enddo

        case (RCI_DOT)
            resvec(1) = 0.0_r8
            do itj = 1, iS%n
                do iti = 1, iS%m
                    resvec(1) = resvec(1) &
                                + (Work(iS%Aidx)%Mat(iti, itj) &
                                   *Work(iS%Bidx)%Mat(iti, itj))
                enddo
            enddo

        case (RCI_HEGV)
            lWorktmp = max(1, 3*iS%n - 1)
            allocate (Worktmp(lWorktmp))
            call dsygv(1, iS%jobz, iS%uplo, iS%n, &
                       Work(iS%Aidx)%Mat, iS%lda, &
                       Work(iS%Bidx)%Mat, iS%ldb, &
                       resvec, Worktmp, lWorktmp, info)
            if (info /= 0) then
                print *, '!!!HEGV Fail: ', info
                stop
            end if
            deallocate (Worktmp)

        case (RCI_COLSCALE)
            do it = 1, iS%n
                Work(iS%Aidx)%Mat(:, it) &
                    = resvec(it)*Work(iS%Aidx)%Mat(:, it)
            enddo

        case (RCI_COL_NORM)
            do it = 1, iS%n
                resvec(it) = norm2(Work(iS%Aidx)%Mat(1:iS%m, it))
            enddo

        case (RCI_SUBCOL)
            iti = 0
            do it = 1, iS%n
                if (resvec(it) > 0.5_r8) then
                    iti = iti + 1
                    Work(iS%Bidx)%Mat(:, iti) = Work(iS%Aidx)%Mat(:, it)
                end if
            enddo

        case (RCI_SUBROW)
            iti = 0
            do it = 1, iS%m
                if (resvec(it) > 0.5_r8) then
                    iti = iti + 1
                    Work(iS%Bidx)%Mat(iti,:) = Work(iS%Aidx)%Mat(it,:)
                end if
            enddo

        case (RCI_POTRF)
            call dpotrf(iS%uplo, iS%n, Work(iS%Aidx)%Mat, iS%lda, info)
            if (info /= 0) then
                print *, '!!!POTRF Fail: ', info
                stop
            end if

        case (RCI_TRSM)
            call dtrsm(iS%side, iS%uplo, iS%trA, 'N', iS%m, iS%n, &
                iS%alpha, Work(iS%Aidx)%Mat, iS%lda, &
                Work(iS%Bidx)%Mat, iS%ldb)

        case default
        end select

    end do

    ijob = RCI_INIT_IJOB
    do
        call rci_solve_deallocate(r_h, ijob, iS, task)
        select case (task)
        case (RCI_NULL)
        case (RCI_STOP)
            exit
        case (RCI_DEALLOCATE)
            deallocate (Work(iS%Aidx)%Mat)
        case default
        end select
    end do

    deallocate (resvec)
    deallocate (Work)

    call rci_get_time(t2)
    call rci_read_clean()

    write (*, '("  Finished SCF")')
    write (*, '("  | Time       :",F16.3,"s")') t2 - t1
    write (*, '("  | Min Energy :",F16.8 )') r_h%total_energy
    write (*, '("  | Num Iter   :",I16 )') r_h%total_iter
    write (*, *)
    write (*, '("  Finished test program")')
    write (*, '("    The reference energy is calculated with 112 states")')
    if (abs((r_h%total_energy - e_ref)/e_ref) < e_tol) then
        write (*, '("  Passed.")')
    else
        write (*, '("  Failed!")')
    endif
    write (*, *)

end subroutine
