! Copyright (c) 2015-2019, the ELSI team.
! All rights reserved.
!
! This file is part of ELSI and is distributed under the BSD 3-clause license,
! which may be found in the LICENSE file in the ELSI root directory.

!>
!! This module provides subroutines for accurate timing.
!!
module ELSI_RCI_TIMER

   use ELSI_RCI_PRECISION, only: r8,i4,i8

   implicit none

   private

   public :: rci_init_timer
   public :: rci_get_time

   integer(i4) :: clock_rate

contains

subroutine rci_init_timer()

   implicit none

   integer(i4) :: initial_time
   integer(i4) :: clock_max

   call system_clock(initial_time,clock_rate,clock_max)

end subroutine

subroutine rci_get_time(wtime)

   implicit none

   real(r8), intent(out) :: wtime

   integer(i4) :: tics

   call system_clock(tics)

   wtime = 1.0_r8*tics/clock_rate

end subroutine

end module ELSI_RCI_TIMER
