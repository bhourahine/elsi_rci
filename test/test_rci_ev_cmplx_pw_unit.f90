! Copyright (c) 2015-2019, the ELSI team.
! All rights reserved.
!
! This file is part of ELSI and is distributed under the BSD 3-clause license,
! which may be found in the LICENSE file in the ELSI root directory.

!>
!! This subroutine tests complex iterative eigensolver, BLACS_DENSE format.
!!
subroutine test_rci_ev_cmplx_pw_unit(solver,h_file)

    use ELSI_RCI ! User should use this
    use ELSI_RCI_PRECISION ! Only for test
    use ELSI_RCI_READ_MAT ! Only for test
    use ELSI_RCI_WRITE_MAT ! Only for test
    use ELSI_RCI_TIMER ! Only for test
    use MKL_DFTI

    implicit none

    integer(i4), intent(in) :: solver
    character(*), intent(in) :: h_file

    ! Reference values
    real(r8), parameter :: e_elpa  = 1.09980681_r8

    integer(i4) :: n_state = 0
    integer(i4) :: task
    integer(i4) :: ijob
    integer(i4) :: it, iti, itj
    integer(i4) :: lWorktmp, info
    integer(i4) :: lda, ldb, ldc

    complex(r8) :: COMP0 = cmplx(0.0_r8, 0.0_r8, r8)
    complex(r8) :: COMP1 = cmplx(1.0_r8, 0.0_r8, r8)

    integer(i4) :: max_iter = 200
    real(r8) :: tol_iter = 2.e-10_r8
    integer(i4) :: verbose = 1
    real(r8) :: t1
    real(r8) :: t2
    real(r8) :: rnum1
    real(r8) :: rnum2

    real(r8) :: e_test
    real(r8) :: e_ref
    real(r8) :: e_tol = 8.e-1_r8

    type(rci_handle) :: r_h

    type(rci_instr) :: iS
    real(r8), allocatable :: resvec(:)

    !preconditioner
    complex(r8) :: x, denm

    complex(r8), allocatable :: Mattmp(:, :)
    complex(r8), allocatable :: Worktmp(:)
    complex(r8), allocatable :: HWorktmp(:,:)
    real(r8), allocatable :: RWorktmp(:)

    type :: matrix
        complex(r8), allocatable :: Mat(:, :)
    end type matrix

    type(matrix), allocatable :: Work(:)

    complex, allocatable :: psi(:)
    type(DFTI_DESCRIPTOR), pointer :: desc_handle_fft
    integer(i4) :: fftstate

    write (*, '("  ####################################")')
    write (*, '("  ##   ELSI RCI REAL TEST PROGRAMS  ##")')
    write (*, '("  ####################################")')
    write (*, *)
    write (*, '("  This test program performs the following steps:")')
    write (*, '("  1) Reads Hamiltonian and overlap matrices;")')
    if (solver == RCI_SOLVER_DAVIDSON) then
        write (*, '("  2) Computes the eigenfunc with RCI_DAVIDSON.")')
    endif
    if (solver == RCI_SOLVER_OMM) then
        write (*, '("  2) Computes the eigenfunc with RCI_OMM.")')
    endif
    if (solver == RCI_SOLVER_PPCG) then
        write (*, '("  2) Computes the eigenfunc with RCI_PPCG.")')
    endif
    if (solver == RCI_SOLVER_CHEBFILTER) then
        write (*, '("  2) Computes the eigenfunc with RCI_CHEBFILTER.")')
    endif
    write (*, *)

    e_ref = e_elpa

    call rci_init_timer()
    call rci_get_time(t1)

    call rci_read_pw(h_file, .false.)
    n_state = int(n_electron/2,i4)

    call rci_get_time(t2)

    write (*, '("  Finished reading H matrix")')
    write (*, '("  | Time       :",F10.3,"s")') t2 - t1
    write (*, '("  | n_Basis    :",I10 )') n_basis
    write (*, '("  | n_Basis_pw :",I10 )') n_basis_pw
    write (*, '("  | nState     :",I10 )') n_state
    write (*, *)

    call rci_get_time(t1)

    call rci_init(r_h, solver, n_basis_pw, n_state, tol_iter, max_iter, &
        verbose)
    r_h%ovlp_is_unit = .true.

    ! Set the estimated lower and upper bounds for the spectrum
    r_h%cheb_est_lb = 0.25_r8
    r_h%cheb_est_ub = 20.0_r8
    r_h%omm_est_ub = 20.0_r8

    allocate (Work(100))

    ijob = RCI_INIT_IJOB
    do
        call rci_solve_allocate(r_h, ijob, iS, task)
        select case (task)
        case (RCI_NULL)
        case (RCI_STOP)
            exit
        case (RCI_ALLOCATE)
            allocate (Work(iS%Aidx)%Mat(iS%m, iS%n))
            Work(iS%Aidx)%Mat = COMP0
        case default
        end select
    end do

    ! Initialize wave functions
    do itj = 1, n_state
        do iti = 1, n_basis_pw
            call random_number(rnum1)
            call random_number(rnum2)
            Work(1)%Mat(iti, itj) = cmplx(rnum1*0.01_r8 &
                                          /sqrt(real(n_basis_pw, r8)), &
                                          rnum2*0.01_r8 &
                                          /sqrt(real(n_basis_pw, r8)), r8)
        end do
    end do

    ! Prepare for FFT
    fftstate = DftiCreateDescriptor( desc_handle_fft, DFTI_SINGLE, &
                               DFTI_COMPLEX, 3, n_basis_1D )
    fftstate = DftiCommitDescriptor( desc_handle_fft )
    allocate (psi(n_basis))

    allocate (HWorktmp(n_h_vnl,r_h%max_n))
    allocate (resvec(r_h%n_res))
    resvec = 0.0_r8
    ijob = RCI_INIT_IJOB

    do
        call rci_solve(r_h, ijob, iS, task, resvec)

        select case (task)
        case (RCI_NULL)
        case (RCI_STOP)
            write (*, *) 'Maximum iteration number is reached.'
            exit
        case (RCI_CONVERGE)
            exit

        case (RCI_H_MULTI)
            ! KinA = gkin * A
            do iti = 1, n_basis_pw
                Work(iS%Bidx)%Mat(iti,:) = (h_gkin(iti)) &
                                         * Work(iS%Aidx)%Mat(iti,:)
            end do

            ! VtotA = fft( vtot * ifft(A))
            do it = 1, iS%n
                psi = COMP0
                do iti = 1, n_basis_pw
                    psi(h_idx(iti)) = Work(iS%Aidx)%Mat(iti,it)
                end do

                fftstate = DftiComputeBackward( desc_handle_fft, psi )
                psi = psi * h_vtot / n_basis
                fftstate = DftiComputeForward( desc_handle_fft, psi )

                do iti = 1, n_basis_pw
                    Work(iS%Bidx)%Mat(iti,it) = Work(iS%Bidx)%Mat(iti,it) &
                                              + psi(h_idx(iti))
                end do
            end do

            ! VnlA = vnlmat*vnlsign*vnlmat'*A
            lda = n_basis_pw
            ldb = n_basis_pw
            call zgemm('C', 'N', n_h_vnl, iS%n, iS%m, COMP1, &
                       h_vnlmat, n_basis_pw, Work(iS%Aidx)%Mat, lda, &
                       COMP0, &
                       HWorktmp, n_h_vnl)
            do iti = 1, n_h_vnl
                HWorktmp(iti,:) = h_vnlsign(iti)*HWorktmp(iti,:)
            end do
            call zgemm('N', 'N', n_basis_pw, iS%n, n_h_vnl, COMP1, &
                       h_vnlmat, n_basis_pw, HWorktmp, n_h_vnl, &
                       COMP1, &
                       Work(iS%Bidx)%Mat, ldb)

        case (RCI_P_MULTI)
            if ((solver == RCI_SOLVER_OMM) &
            .or. (solver == RCI_SOLVER_CHEBFILTER)) then
                ! No preconditioner
                Work(iS%Bidx)%Mat = Work(iS%Aidx)%Mat
            else
                ! Kerker preconditioner
                do itj = 1, iS%n
                    do iti = 1, iS%m
                        x = h_gkin(iti) - resvec(itj)
                        denm = 0.5_r8 * (1.0_r8 + x &
                            + sqrt(1.0_r8 + (x - 1.0_r8) * (x - 1.0_r8)))
                        Work(iS%Bidx)%Mat(iti, itj) &
                            = - Work(iS%Aidx)%Mat(iti, itj)/denm
                    end do
                end do
            end if

        case (RCI_GEMM)
            call zgemm(iS%TrA, iS%TrB, iS%m, iS%n, iS%k, &
                       cmplx(iS%alpha, 0.0_r8, r8), &
                       Work(iS%Aidx)%Mat, iS%lda, &
                       Work(iS%Bidx)%Mat, iS%ldb, &
                       cmplx(iS%beta, 0.0_r8, r8), &
                       Work(iS%Cidx)%Mat, iS%ldc)

        case (RCI_AXPY)
            call zaxpy(iS%m*iS%n, cmplx(iS%alpha, 0.0_r8, r8), &
                       Work(iS%Aidx)%Mat, 1, &
                       Work(iS%Bidx)%Mat, 1)

        case (RCI_COPY)
            if (iS%TrA == 'N') then
                Work(iS%Bidx)%Mat = Work(iS%Aidx)%Mat
            else
                Work(iS%Bidx)%Mat = conjg(transpose(Work(iS%Aidx)%Mat))
            endif

        case (RCI_SUBCOPY)
            Work(iS%Bidx)%Mat(iS%rBoff + 1:iS%rBoff + iS%m, &
                              iS%cBoff + 1:iS%cBoff + iS%n) &
                = Work(iS%Aidx)%Mat(iS%rAoff + 1:iS%rAoff + iS%m, &
                                    iS%cAoff + 1:iS%cAoff + iS%n)

        case (RCI_SCALE)
            Work(iS%Aidx)%Mat = cmplx(iS%alpha, 0.0_r8, r8) &
                                *Work(iS%Aidx)%Mat

        case (RCI_TRACE)
            resvec(1) = 0.0_r8
            do it = 1, iS%n
                resvec(1) = resvec(1) &
                            + real(Work(iS%Aidx)%Mat(it, it), r8)
            enddo

        case (RCI_DOT)
            resvec(1) = 0.0_r8
            do itj = 1, iS%n
                do iti = 1, iS%m
                    resvec(1) = resvec(1) &
                                + real(conjg( &
                                       Work(iS%Aidx)%Mat(iti, itj)) &
                                       *Work(iS%Bidx)%Mat(iti, itj), r8)
                enddo
            enddo

        case (RCI_HEGV)
            lWorktmp = max(1, 2*iS%n - 1)
            allocate (Worktmp(lWorktmp))
            allocate (RWorktmp(max(1,3*iS%n-2)))
            call zhegv(1, iS%jobz, iS%uplo, iS%n, &
                       Work(iS%Aidx)%Mat, iS%lda, &
                       Work(iS%Bidx)%Mat, iS%ldb, &
                       resvec, Worktmp, lWorktmp, RWorktmp, info)
            if (info /= 0) then
                print *, '!!!HEGV Fail: ', info, ', @ijob: ', ijob
                stop
            end if
            deallocate (Worktmp)
            deallocate (RWorktmp)

        case (RCI_COLSCALE)
            do it = 1, iS%n
                Work(iS%Aidx)%Mat(:, it) &
                    = resvec(it)*Work(iS%Aidx)%Mat(:, it)
            enddo

        case (RCI_COL_NORM)
            do it = 1, iS%n
                resvec(it) = COMP0
                do iti = 1, iS%m
                    resvec(it) = resvec(it) &
                                + real(conjg( &
                                       Work(iS%Aidx)%Mat(iti, it)) &
                                       *Work(iS%Aidx)%Mat(iti, it), r8)
                enddo
            enddo

        case (RCI_SUBCOL)
            iti = 0
            do it = 1, iS%n
                if (resvec(it) > 0.5_r8) then
                    iti = iti + 1
                    Work(iS%Bidx)%Mat(:, iti) = Work(iS%Aidx)%Mat(:, it)
                end if
            enddo

        case (RCI_SUBROW)
            iti = 0
            do it = 1, iS%m
                if (resvec(it) > 0.5_r8) then
                    iti = iti + 1
                    Work(iS%Bidx)%Mat(iti,:) = Work(iS%Aidx)%Mat(it,:)
                end if
            enddo

        case (RCI_POTRF)
            call zpotrf(iS%uplo, iS%n, Work(iS%Aidx)%Mat, iS%lda, info)
            if (info /= 0) then
                print *, '!!!POTRF Fail: ', info
                stop
            end if

        case (RCI_TRSM)
            call ztrsm(iS%side, iS%uplo, iS%trA, 'N', iS%m, iS%n, &
                iS%alpha, Work(iS%Aidx)%Mat, iS%lda, &
                Work(iS%Bidx)%Mat, iS%ldb)

        case default
        end select

    end do

    ijob = RCI_INIT_IJOB
    do
        call rci_solve_deallocate(r_h, ijob, iS, task)
        select case (task)
        case (RCI_NULL)
        case (RCI_STOP)
            exit
        case (RCI_DEALLOCATE)
            deallocate (Work(iS%Aidx)%Mat)
        case default
        end select
    end do

    deallocate (resvec)
    deallocate (Work)
    deallocate (HWorktmp)
    fftstate = DftiFreeDescriptor(desc_handle_fft)

    call rci_get_time(t2)
    call rci_read_clean()

    write (*, '("  Finished SCF")')
    write (*, '("  | Time       :",F16.3,"s")') t2 - t1
    write (*, '("  | Min Energy :",F16.8 )') r_h%total_energy
    write (*, '("  | Num Iter   :",I16 )') r_h%total_iter
    write (*, *)
    write (*, '("  Finished test program")')
    write (*, '("    The reference energy is calculated with 8 states")')
    if (abs((r_h%total_energy - e_ref)/e_ref) < e_tol) then
        write (*, '("  Passed.")')
    else
        write (*, '("  Failed!")')
    endif
    write (*, *)

end subroutine
