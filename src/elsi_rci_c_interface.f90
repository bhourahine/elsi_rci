! Copyright (c) 2015-2021, the ELSI team.
! All rights reserved.
!
! This file is part of ELSI and is distributed under the BSD 3-clause license,
! which may be found in the LICENSE file in the ELSI root directory.

!>
!! This module provides public-facting interface routines for using ELSI RCI.
!!
module ELSI_RCI_C_INTERFACE

    use, intrinsic :: ISO_C_BINDING
    use ELSI_RCI
    use ELSI_RCI_CONSTANTS

    implicit none

    integer(kind=c_int), bind(C) :: RCI_SOLVER_c           
    integer(kind=c_int), bind(C) :: RCI_SOLVER_DAVIDSON_c  
    integer(kind=c_int), bind(C) :: RCI_SOLVER_OMM_c       
    integer(kind=c_int), bind(C) :: RCI_SOLVER_PPCG_c      
    integer(kind=c_int), bind(C) :: RCI_SOLVER_CHEBFILTER_c
    integer(kind=c_int), bind(C) :: RCI_INIT_IJOB_c        
    integer(kind=c_int), bind(C) :: RCI_NULL_c             
    integer(kind=c_int), bind(C) :: RCI_STOP_c             
    integer(kind=c_int), bind(C) :: RCI_CONVERGE_c         
    integer(kind=c_int), bind(C) :: RCI_ALLOCATE_c         
    integer(kind=c_int), bind(C) :: RCI_DEALLOCATE_c       
    integer(kind=c_int), bind(C) :: RCI_H_MULTI_c          
    integer(kind=c_int), bind(C) :: RCI_S_MULTI_c          
    integer(kind=c_int), bind(C) :: RCI_P_MULTI_c          
    integer(kind=c_int), bind(C) :: RCI_COPY_c             
    integer(kind=c_int), bind(C) :: RCI_SUBCOPY_c          
    integer(kind=c_int), bind(C) :: RCI_SUBCOL_c           
    integer(kind=c_int), bind(C) :: RCI_SUBROW_c           
    integer(kind=c_int), bind(C) :: RCI_TRACE_c            
    integer(kind=c_int), bind(C) :: RCI_DOT_c              
    integer(kind=c_int), bind(C) :: RCI_SCALE_c            
    integer(kind=c_int), bind(C) :: RCI_COLSCALE_c         
    integer(kind=c_int), bind(C) :: RCI_ROWSCALE_c         
    integer(kind=c_int), bind(C) :: RCI_AXPY_c             
    integer(kind=c_int), bind(C) :: RCI_COL_NORM_c         
    integer(kind=c_int), bind(C) :: RCI_GEMM_c             
    integer(kind=c_int), bind(C) :: RCI_HEEV_c             
    integer(kind=c_int), bind(C) :: RCI_HEGV_c             
    integer(kind=c_int), bind(C) :: RCI_POTRF_c            
    integer(kind=c_int), bind(C) :: RCI_TRSM_c             

    private

contains

    !>
    !! Convert a C string into a Fortran string. A Fortran string is NOT a character
    !! array without a NULL character.
    !!
    subroutine str_c2f(str_c,str_f)
    
       implicit none
    
       character(kind=c_char,len=1), intent(in) :: str_c(*)
       character(len=:), allocatable, intent(out) :: str_f
    
       integer(kind=c_int) :: str_f_len
    
       str_f_len = 0
    
       do
         if(str_c(str_f_len+1) == C_NULL_CHAR) then
            exit
         end if
    
         str_f_len = str_f_len+1
       end do
    
       allocate(character(len=str_f_len) :: str_f)
    
       str_f(:) = transfer(str_c(1:str_f_len),str_f)
    
    end subroutine

    subroutine c_rci_init(r_h_c, iS_c, solver, n_basis, n_state, tol_iter, &
            max_iter, verbose) bind(C)

        implicit none

        type(c_ptr) :: r_h_c
        type(c_ptr) :: iS_c
        integer(kind=c_int), value, intent(in)       :: solver
        integer(kind=c_int), value, intent(in)       :: n_basis !< Number of basis
        integer(kind=c_int), value, intent(in)       :: n_state !< Number of states
        real(kind=c_double), value, intent(in)       :: tol_iter !< Number of basis
        integer(kind=c_int), value, intent(in)       :: max_iter !< Number of states
        integer(kind=c_int), value, intent(in)       :: verbose !< output

        type(rci_handle), pointer :: r_h_f
        type(rci_instr), pointer :: iS_f

        allocate(r_h_f)
        allocate(iS_f)

        call rci_init(r_h_f, solver, n_basis, n_state, tol_iter, max_iter, verbose)

        r_h_c = c_loc(r_h_f)
        iS_c = c_loc(iS_f)

        RCI_SOLVER_c            = RCI_SOLVER            
        RCI_SOLVER_DAVIDSON_c   = RCI_SOLVER_DAVIDSON  
        RCI_SOLVER_OMM_c        = RCI_SOLVER_OMM       
        RCI_SOLVER_PPCG_c       = RCI_SOLVER_PPCG      
        RCI_SOLVER_CHEBFILTER_c = RCI_SOLVER_CHEBFILTER
        RCI_INIT_IJOB_c         = RCI_INIT_IJOB        
        RCI_NULL_c              = RCI_NULL             
        RCI_STOP_c              = RCI_STOP             
        RCI_CONVERGE_c          = RCI_CONVERGE         
        RCI_ALLOCATE_c          = RCI_ALLOCATE         
        RCI_DEALLOCATE_c        = RCI_DEALLOCATE       
        RCI_H_MULTI_c           = RCI_H_MULTI          
        RCI_S_MULTI_c           = RCI_S_MULTI          
        RCI_P_MULTI_c           = RCI_P_MULTI          
        RCI_COPY_c              = RCI_COPY             
        RCI_SUBCOPY_c           = RCI_SUBCOPY          
        RCI_SUBCOL_c            = RCI_SUBCOL           
        RCI_SUBROW_c            = RCI_SUBROW           
        RCI_TRACE_c             = RCI_TRACE            
        RCI_DOT_c               = RCI_DOT              
        RCI_SCALE_c             = RCI_SCALE            
        RCI_COLSCALE_c          = RCI_COLSCALE         
        RCI_ROWSCALE_c          = RCI_ROWSCALE         
        RCI_AXPY_c              = RCI_AXPY             
        RCI_COL_NORM_c          = RCI_COL_NORM         
        RCI_GEMM_c              = RCI_GEMM             
        RCI_HEEV_c              = RCI_HEEV             
        RCI_HEGV_c              = RCI_HEGV             
        RCI_POTRF_c             = RCI_POTRF            
        RCI_TRSM_c              = RCI_TRSM             

    end subroutine


    subroutine c_rci_solve_allocate(r_h_c, ijob, iS_c, task) bind(C)

        implicit none

        !**** INPUT ***********************************!
        type(c_ptr), value, intent(in) :: r_h_c
        type(rci_handle), pointer      :: r_h_f
        type(c_ptr), value, intent(in) :: iS_c
        type(rci_instr), pointer       :: iS_f

        !**** OUTPUT **********************************!
        integer(kind=c_int), intent(out) :: task

        !**** INOUT ***********************************!
        integer(kind=c_int), intent(inout) :: ijob ! job id

        call c_f_pointer(r_h_c, r_h_f)
        call c_f_pointer(iS_c, iS_f)

        call rci_solve_allocate(r_h_f, ijob, iS_f, task)

    end subroutine

    subroutine c_rci_solve_deallocate(r_h_c, ijob, iS_c, task) bind(C)

        implicit none

        !**** INPUT ***********************************!
        type(c_ptr), value, intent(in) :: r_h_c
        type(rci_handle), pointer      :: r_h_f
        type(c_ptr), value, intent(in) :: iS_c
        type(rci_instr), pointer :: iS_f

        !**** OUTPUT **********************************!
        integer(kind=c_int), intent(out) :: task

        !**** INOUT ***********************************!
        integer(kind=c_int), intent(inout) :: ijob ! job id

        call c_f_pointer(r_h_c, r_h_f)
        call c_f_pointer(iS_c, iS_f)

        call rci_solve_deallocate(r_h_f, ijob, iS_f, task)

    end subroutine

    subroutine c_rci_solve(r_h_c, ijob, iS_c, task, resvec_c) bind(C)

        implicit none

        !**** INPUT ***********************************!
        type(c_ptr), value, intent(in) :: r_h_c
        type(rci_handle), pointer      :: r_h_f
        type(c_ptr), value, intent(in) :: iS_c
        type(rci_instr), pointer       :: iS_f

        !**** OUTPUT **********************************!
        integer(kind=c_int), intent(out) :: task

        !**** INOUT ***********************************!
        integer(kind=c_int), intent(inout) :: ijob ! job id
        type(c_ptr), intent(inout) :: resvec_c
        real(kind=c_double), pointer :: resvec_f(:)
        integer(kind=c_int) :: n_res

        call c_f_pointer(r_h_c, r_h_f)
        call c_f_pointer(iS_c, iS_f)

        n_res = r_h_f%n_res

        call c_f_pointer(resvec_c, resvec_f, shape=[n_res])

        call rci_solve(r_h_f, ijob, iS_f, task, resvec_f)

    end subroutine

end module ELSI_RCI_C_INTERFACE
