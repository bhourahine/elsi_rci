! Copyright (c) 2015-2019, the ELSI team.
! All rights reserved.
!
! This file is part of ELSI and is distributed under the BSD 3-clause license,
! which may be found in the LICENSE file in the ELSI root directory.

!>
!! This module contains variables accessible in ELSI RCI and related modules.
module ELSI_RCI_DATATYPE
    use, intrinsic :: ISO_C_BINDING
    use ELSI_RCI_PRECISION
    use ELSI_RCI_CONSTANTS

    implicit none

    type, bind(C), public :: rci_handle

        ! Systems
        integer(i4) :: n_basis
        integer(i4) :: n_state
        integer(i4) :: max_n
        logical     :: ovlp_is_unit = .false.

        ! Iteration
        integer(i4) :: solver = RCI_SOLVER
        integer(i4) :: max_iter = 1000
        integer(i4) :: n_res ! size of resvec not specified by user
        real(r8)    :: tol_iter = 1E-6
        integer(i4) :: verbose = 0 ! output verbose

        ! Output
        integer(i4) :: total_iter
        real(r8) :: total_energy

        ! OMM
        real(r8) :: omm_est_ub

        ! PPCG
        integer(i4) :: ppcg_sbsize
        integer(i4) :: ppcg_rrstep
        real(r8) :: ppcg_tol_lock

        ! ChebFilter
        real(r8) :: cheb_est_lb
        real(r8) :: cheb_est_ub
        integer(i4) :: cheb_max_inneriter


    end type

    ! RCI Instruction type
    ! H: Hamiltonian; S: Overlapping matrix; P: Preconditioner
    ! A, B, C: General matrices
    type, bind(C), public :: rci_instr

        character   :: jobz, uplo ! job char; and upper or lower
        character   :: side ! left or right side
        character   :: trH, trS, trP, trA, trB ! Operation for H, S, P, A, B

        integer(i4) :: m, n ! size of the output matrix
        integer(i4) :: k ! size of the intermediate multiplication
        integer(i4) :: lda, ldb, ldc ! leading size for matrix A, B and C

        integer(i4) :: rAoff,cAoff ! row and column offset of A
        integer(i4) :: rBoff,cBoff ! row and column offset of B

        integer(i4) :: Aidx, Bidx, Cidx ! indices for matrix A, B and C

        real(r8)  :: alpha, beta ! coefficients

    end type

end module ELSI_RCI_DATATYPE
