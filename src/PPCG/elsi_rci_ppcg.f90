! Copyright (c) 2015-2019, the ELSI team.
! All rights reserved.
!
! This file is part of ELSI and is distributed under the BSD 3-clause license,
! which may be found in the LICENSE file in the ELSI root directory.

!>
!! This module contains subroutines to solve an eigenproblem using a reverse
!! communication interface.
!!
module ELSI_RCI_PPCG

    use ELSI_RCI_OPS
    use ELSI_RCI_CONSTANTS
    use ELSI_RCI_DATATYPE
    use ELSI_RCI_PRECISION, only:i4

    implicit none

    public :: rci_ppcg
    public :: rci_ppcg_allocate
    public :: rci_ppcg_deallocate

    ! Matrix ID of size m by n
    !&<
    integer(i4), parameter :: MID_Psi       = 1
    integer(i4), parameter :: MID_Psi_lock  = 2
    integer(i4), parameter :: MID_HPsi      = 3
    integer(i4), parameter :: MID_HPsi_lock = 4
    integer(i4), parameter :: MID_W         = 5
    integer(i4), parameter :: MID_HW        = 6
    integer(i4), parameter :: MID_P         = 7
    integer(i4), parameter :: MID_HP        = 8
    integer(i4), parameter :: MID_PWORK     = 9
    integer(i4), parameter :: MID_PWORK0    = 10
    integer(i4), parameter :: MID_PWORK1    = 11
    integer(i4), parameter :: MID_PWORK2    = 12
    integer(i4), parameter :: MID_SPsi      = 13
    integer(i4), parameter :: MID_SPsi_lock = 14
    integer(i4), parameter :: MID_SW        = 15
    integer(i4), parameter :: MID_SP        = 16
    integer(i4), parameter :: MID_PWORK3    = 17
    !&>

    ! Matrix ID of size n by n
    !&<
    integer(i4), parameter :: MID_G     = 21
    integer(i4), parameter :: MID_WORK  = 22
    !&>

    ! Stage ID
    !&<
    integer(i4), parameter :: SID_INIT        = 0
    integer(i4), parameter :: SID_LOCK        = 100
    integer(i4), parameter :: SID_ITER        = 200
    integer(i4), parameter :: SID_SEQ_DIAG    = 300
    integer(i4), parameter :: SID_ORTH        = 400
    integer(i4), parameter :: SID_RR          = 500
    integer(i4), parameter :: SID_NORR        = 600
    integer(i4), parameter :: SID_FINISH      = 700
    integer(i4), parameter :: SID_ALLOCATE    = 800
    integer(i4), parameter :: SID_DEALLOCATE  = 900
    !&>

contains

    subroutine rci_ppcg_allocate(r_h, ijob, iS, task)

        implicit none

        !**** INPUT ***********************************!
        type(rci_handle), intent(in) :: r_h

        !**** OUTPUT **********************************!
        integer(i4), intent(out) :: task

        !**** INOUT ***********************************!
        integer(i4), intent(inout) :: ijob ! job id
        type(rci_instr), intent(inout) :: iS

        !**** LOCAL ***********************************!
        integer(i4), save :: iter ! step num.
        integer(i4), save :: m, n ! size of the wave functions.

        if (ijob <= SID_ALLOCATE) then
            ijob = SID_ALLOCATE + 1
            m = r_h%n_basis
            n = r_h%max_n
            iter = 0
        end if

        ! Allocate matrix of size m by n
        if (ijob == SID_ALLOCATE + 1) then
            iter = iter + 1
            if (iter > 17) then
                ijob = ijob + 1
                iter = 20
                call rci_op_null(task)
            elseif ((iter > 12) .and. (r_h%ovlp_is_unit)) then
                ijob = ijob + 1
                iter = 20
                call rci_op_null(task)
            else
                call rci_op_allocate(iS, task, m, n, iter)
            end if
            return
        end if

        ! Allocate matrix of size n by n
        if (ijob == SID_ALLOCATE + 2) then
            iter = iter + 1
            if (iter > 22) then
                call rci_op_stop(task)
            else
                call rci_op_allocate(iS, task, n, n, iter)
            end if
            return
        end if

    end subroutine

    subroutine rci_ppcg_deallocate(r_h, ijob, iS, task)

        implicit none

        !**** INPUT ***********************************!
        type(rci_handle), intent(in) :: r_h

        !**** OUTPUT **********************************!
        integer(i4), intent(out) :: task

        !**** INOUT ***********************************!
        integer(i4), intent(inout) :: ijob ! job id
        type(rci_instr), intent(inout) :: iS

        !**** LOCAL ***********************************!
        integer(i4), save :: iter ! step num.

        if (ijob <= SID_DEALLOCATE) then
            ijob = SID_DEALLOCATE + 1
            iter = 0
        end if

        ! Deallocate matrix of size m by n
        if (ijob == SID_DEALLOCATE + 1) then
            iter = iter + 1
            if (iter > 17) then
                ijob = ijob + 1
                iter = 20
                call rci_op_null(task)
            elseif ((iter > 12) .and. (r_h%ovlp_is_unit)) then
                ijob = ijob + 1
                iter = 20
                call rci_op_null(task)
            else
                call rci_op_deallocate(iS, task, iter)
            end if
            return
        end if

        ! Deallocate matrix of size n by n
        if (ijob == SID_DEALLOCATE + 2) then
            iter = iter + 1
            if (iter > 22) then
                call rci_op_stop(task)
            else
                call rci_op_deallocate(iS, task, iter)
            end if
            return
        end if

    end subroutine

    subroutine rci_ppcg(r_h, ijob, iS, task, resvec)

        implicit none

        !**** INPUT ***********************************!

        !**** OUTPUT **********************************!
        integer(i4), intent(out) :: task

        !**** INOUT ***********************************!
        type(rci_handle), intent(inout) :: r_h
        integer(i4), intent(inout) :: ijob ! job id
        real(r8), intent(inout) :: resvec(:)
        type(rci_instr), intent(inout) :: iS

        !**** LOCAL ***********************************!
        integer(i4) :: it, iti
        integer(i4), save :: m ! size of basis
        integer(i4), save :: n ! number of current states
        integer(i4), save :: max_n ! max number of columns
        integer(i4), save :: iter ! iteration num.
        integer(i4), save :: iter_diag ! iteration num.
        integer(i4), save :: max_iter ! max number of iterations
        integer(i4), save :: rrstep ! iteration number gap between RR step
        integer(i4), save :: sbsize ! size of the sequential diag block
        integer(i4), save :: sbsize_last ! size of the last block
        integer(i4), save :: lsize ! local size of the block
        integer(i4), save :: lsize3 ! size of combined block
        integer(i4), save :: nsb ! number of the blocks
        integer(i4), save :: nact ! number of active columns

        real(r8), save :: tol_iter ! convergence tolerance
        real(r8), save :: tol_lock ! locking tolerance
        real(r8), save :: trG ! trace value of the overlapping matrix
        real(r8), save :: trG_old ! previous trace
        real(r8), save :: trdiff ! difference of the trace

        logical, save :: ovlp_is_unit ! ovlp is unit flag
        logical, save :: conv_flag = .false. ! indicator for convergence

        integer(i4), save, allocatable :: act_idx(:)

        !**********************************************!

        ! -- Init: parameter setup
        if (ijob <= SID_INIT) then
            m = r_h%n_basis
            n = r_h%n_state
            max_n = r_h%max_n
            ovlp_is_unit = r_h%ovlp_is_unit
            max_iter = r_h%max_iter
            tol_iter = r_h%tol_iter
            tol_lock = r_h%ppcg_tol_lock
            sbsize = r_h%ppcg_sbsize
            rrstep = r_h%ppcg_rrstep
            iter = 0

            allocate (act_idx(n))
            act_idx = 0

            call rci_op_null(task)
            ijob = SID_INIT + 1
            return
        end if

        ! -- HPsi = H*Psi
        if (ijob == SID_INIT + 1) then
            call rci_op_h_multi(iS, task, 'N', m, n, MID_Psi, MID_HPsi)
            ijob = ijob + 1
            return
        end if

        ! -- SPsi = S*Psi
        if (ijob == SID_INIT + 2) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_s_multi(iS, task, 'N', m, n, MID_Psi, MID_SPsi)
            end if
            ijob = ijob + 1
            return
        end if

        ! -- G = Psi'*HPsi
        if (ijob == SID_INIT + 3) then
            call rci_op_gemm(iS, task, 'C', 'N', n, n, m, 1.0_r8, &
                             MID_Psi, m, MID_HPsi, m, 0.0_r8, &
                             MID_G, max_n)
            ijob = ijob + 1
            return
        end if

        ! -- W = HPsi - SPsi*G
        if (ijob == SID_INIT + 4) then
            call rci_op_copy(iS, task, 'N', MID_HPsi, MID_W)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_INIT + 5) then
            if (ovlp_is_unit) then
                call rci_op_gemm(iS, task, 'N', 'N', m, n, n, -1.0_r8, &
                                 MID_Psi, m, MID_G, max_n, 1.0_r8, &
                                 MID_W, m)
            else
                call rci_op_gemm(iS, task, 'N', 'N', m, n, n, -1.0_r8, &
                                 MID_SPsi, m, MID_G, max_n, 1.0_r8, &
                                 MID_W, m)
            end if
            ijob = SID_LOCK
            return
        end if

        ! -------------------------------------------------
        ! - lock converged eigen pairs
        if (ijob == SID_LOCK ) then
            call rci_op_col_norm(iS, task, m, n, MID_W, m)
            ijob = ijob + 1
            return
        end if

        ! - resvec = idx not conv
        if (ijob == SID_LOCK + 1) then
            nact = 0
            do it = 1, n
                if (resvec(it) > tol_lock) then
                    resvec(it) = 1.0_r8
                    act_idx(it) = 1.0_r8
                    nact = nact + 1
                else
                    resvec(it) = 0.0_r8
                    act_idx(it) = 0.0_r8
                end if
            end do

            sbsize_last = sbsize
            nsb = floor(dble(nact)/dble(sbsize))
            if (mod(nact, sbsize) /= 0) then
                sbsize_last = nact - sbsize*nsb
                nsb = nsb + 1
            end if

            call rci_op_null(task)
            if (iter > 0) then
                ijob = ijob + 1
            else
                ijob = ijob + 2
            end if
            return
        end if

        ! - G = Psi'*HPsi
        if (ijob == SID_LOCK + 2) then
            call rci_op_gemm(iS, task, 'C', 'N', n, n, &
                             m, 1.0_r8, &
                             MID_Psi, m, MID_HPsi, m, 0.0_r8, &
                             MID_G, max_n)
            ijob = ijob + 1
            return
        end if


        ! G = G(not conv, not conv)
        if (ijob == SID_LOCK + 3) then
            call rci_op_subcol(iS, task, n, n, &
                MID_G, MID_WORK)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_LOCK + 4) then
            call rci_op_subrow(iS, task, n, nact, &
                MID_WORK, MID_G)
            ijob = ijob + 1
            return
        end if

        ! - trG = trace(G)
        if (ijob == SID_LOCK + 5) then
            call rci_op_trace(iS, task, nact, MID_G, max_n)
            ijob = ijob + 1
            return
        end if

        ! - check conv
        if (ijob == SID_LOCK + 6) then
            trG = resvec(1)
            trG_old = trG
            if (r_h%verbose > 0 .and. iter > 0) then
                write(*,'(a,i5,a,es15.7e3)') &
                    'Iter ', iter, ', Rel Err: ', trdiff
            end if

            if ((nact == 0) .or. (iter == max_iter)) then
                ijob = SID_FINISH
                call rci_op_null(task)
                return
            end if
            iter = iter + 1
            call rci_op_null(task)
            ijob = ijob + 1
            return
        end if

        ! Psi_lock = Psi(:, conv)
        if (ijob == SID_LOCK + 7) then
            resvec = 1.0_r8 - act_idx
            call rci_op_subcol(iS, task, m, n, MID_Psi, MID_Psi_lock)
            ijob = ijob + 1
            return
        end if

        ! HPsi_lock = HPsi(:, conv)
        if (ijob == SID_LOCK + 8) then
            call rci_op_subcol(iS, task, m, n, MID_HPsi, MID_HPsi_lock)
            ijob = ijob + 1
            return
        end if

        ! SPsi_lock = SPsi_lock
        if (ijob == SID_LOCK + 9) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_subcol(iS, task, m, n, MID_SPsi, MID_SPsi_lock)
            end if
            ijob = ijob + 1
            return
        end if

        ! Psi = Psi(:, not conv)]
        if (ijob == SID_LOCK + 10) then
            resvec = act_idx
            call rci_op_subcol(iS, task, m, n, MID_Psi, MID_PWORK)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_LOCK + 11) then
            call rci_op_copy(iS, task, 'N', MID_PWORK, MID_Psi)
            ijob = ijob + 1
            return
        end if

        ! W = W(:, not conv)]
        if (ijob == SID_LOCK + 12) then
            call rci_op_subcol(iS, task, m, n, MID_W, MID_PWORK)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_LOCK + 13) then
            call rci_op_copy(iS, task, 'N', MID_PWORK, MID_W)
            ijob = ijob + 1
            return
        end if

        ! HPsi = HPsi(:, not conv)]
        if (ijob == SID_LOCK + 14) then
            call rci_op_subcol(iS, task, m, n, MID_HPsi, MID_PWORK)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_LOCK + 15) then
            call rci_op_copy(iS, task, 'N', MID_PWORK, MID_HPsi)
            ijob = ijob + 1
            return
        end if

        ! SPsi = SPsi(:, not conv)]
        if (ijob == SID_LOCK + 16) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_subcol(iS, task, m, n, MID_SPsi, MID_PWORK)
            end if
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_LOCK + 17) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_copy(iS, task, 'N', MID_PWORK, MID_SPsi)
            end if
            ijob = SID_ITER
            return
        end if

        !-------------------------------------------------
        ! This is the main loop of the PPCG algorithm.

        ! W = precond(W)
        if (ijob == SID_ITER) then
            call rci_op_p_multi(iS, task, 'N', m, nact, MID_W, MID_PWork)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_ITER + 1) then
            call rci_op_copy(iS, task, 'N', MID_PWORK, MID_W)
            ijob = ijob + 1
            return
        end if

        ! -- S-orthogonal of W
        ! -- SW = S*W
        if (ijob == SID_ITER + 2) then
            if (ovlp_is_unit) then
                call rci_op_copy(iS, task, 'N', MID_W, MID_PWORK)
            else
                call rci_op_s_multi(iS, task, 'N', m, nact, MID_W, MID_PWORK)
            end if
            ijob = ijob + 1
            return
        end if

        ! -- XSW = X'*SW
        if (ijob == SID_ITER + 3) then
            call rci_op_gemm(iS, task, 'C', 'N', nact, nact, m, 1.0_r8, &
                             MID_Psi, m, MID_PWORK, m, 0.0_r8, &
                             MID_WORK, max_n)
            ijob = ijob + 1
            return
        end if

        ! -- W = W - X*XSW
        if (ijob == SID_ITER + 4) then
            call rci_op_gemm(iS, task, 'N', 'N', m, nact, nact, -1.0_r8, &
                             MID_Psi, m, MID_WORK, max_n, 1.0_r8, &
                             MID_W, m)
            ijob = ijob + 1
            return
        end if

        ! -- XlockSW = Xlock'*SW
        if (ijob == SID_ITER + 5) then
            call rci_op_gemm(iS, task, 'C', 'N', n-nact, nact, m, &
                             1.0_r8, &
                             MID_Psi_lock, m, MID_PWORK, m, 0.0_r8, &
                             MID_WORK, max_n)
            ijob = ijob + 1
            return
        end if

        ! -- W = W - Xlock*XlockSW
        if (ijob == SID_ITER + 6) then
            call rci_op_gemm(iS, task, 'N', 'N', m, nact, n-nact, &
                             -1.0_r8, &
                             MID_Psi_lock, m, MID_WORK, max_n, 1.0_r8, &
                             MID_W, m)
            ijob = ijob + 1
            return
        end if

        ! -- HW = H*W
        if (ijob == SID_ITER + 7) then
            call rci_op_h_multi(iS, task, 'N', m, nact, MID_W, MID_HW)
            ijob = ijob + 1
            return
        end if

        ! -- SW = S*W
        if (ijob == SID_ITER + 8) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_s_multi(iS, task, 'N', m, nact, MID_W, MID_SW)
            end if
            if (iter > 1) then
                ijob = ijob + 1
            else
                ijob = SID_SEQ_DIAG
                iter_diag = 0
            end if
            return
        end if

        ! -- S-orthogonal of P
        ! -- SP = S*P
        if (ijob == SID_ITER + 9) then
            if (ovlp_is_unit) then
                call rci_op_copy(iS, task, 'N', MID_P, MID_PWORK)
            else
                call rci_op_s_multi(iS, task, 'N', m, nact, MID_P, MID_PWORK)
            end if
            ijob = ijob + 1
            return
        end if

        ! -- XSP = X'*SP
        if (ijob == SID_ITER + 10) then
            call rci_op_gemm(iS, task, 'C', 'N', nact, nact, m, 1.0_r8, &
                             MID_Psi, m, MID_PWORK, m, 0.0_r8, &
                             MID_WORK, max_n)
            ijob = ijob + 1
            return
        end if

        ! -- P = P - X*XSP
        if (ijob == SID_ITER + 11) then
            call rci_op_gemm(iS, task, 'N', 'N', m, nact, nact, -1.0_r8, &
                             MID_Psi, m, MID_WORK, max_n, 1.0_r8, &
                             MID_P, m)
            ijob = ijob + 1
            return
        end if

        ! -- XlockSP = Xlock'*SP
        if (ijob == SID_ITER + 12) then
            call rci_op_gemm(iS, task, 'C', 'N', n-nact, nact, m, &
                             1.0_r8, &
                             MID_Psi_lock, m, MID_PWORK, m, 0.0_r8, &
                             MID_WORK, max_n)
            ijob = ijob + 1
            return
        end if

        ! -- P = P - Xlock*XlockSP
        if (ijob == SID_ITER + 13) then
            call rci_op_gemm(iS, task, 'N', 'N', m, nact, n-nact, &
                             -1.0_r8, &
                             MID_Psi_lock, m, MID_WORK, max_n, 1.0_r8, &
                             MID_P, m)
            ijob = ijob + 1
            return
        end if

        ! -- HP = H*P
        if (ijob == SID_ITER + 14) then
            call rci_op_h_multi(iS, task, 'N', m, nact, MID_P, MID_HP)
            ijob = ijob + 1
            return
        end if

        ! -- SP = S*P
        if (ijob == SID_ITER + 15) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_s_multi(iS, task, 'N', m, nact, MID_P, MID_SP)
            end if
            ijob = SID_SEQ_DIAG
            iter_diag = 0
            return
        end if

        ! - Sequential diagonalization
        if (ijob == SID_SEQ_DIAG) then
            iter_diag = iter_diag + 1
            if (iter_diag < nsb) then
                lsize = sbsize
            else
                lsize = sbsize_last
            end if
            call rci_op_null(task)
            ijob = ijob + 1
            return
        end if

        ! - PWORK1 = [Psi(:,(it-1)*sbsize+1:sbsize)]
        if (ijob == SID_SEQ_DIAG + 1) then
            call rci_op_subcopy(iS, task, m, lsize, &
                                MID_Psi, m, 0, (iter_diag-1)*sbsize, &
                                MID_PWORK1, m, 0, 0)
            ijob = ijob + 1
            return
        end if

        ! - PWORK1 = [PWORK1 W(:,(it-1)*sbsize+1:sbsize)]
        if (ijob == SID_SEQ_DIAG + 2) then
            call rci_op_subcopy(iS, task, m, lsize, &
                                MID_W, m, 0, (iter_diag-1)*sbsize, &
                                MID_PWORK1, m, 0, lsize)
            if (iter == 1) then
                ijob = ijob + 2
                lsize3 = 2*lsize
            else
                ijob = ijob + 1
                lsize3 = 3*lsize
            end if
            return
        end if

        ! - PWORK1 = [PWORK1 P(:,(it-1)*sbsize+1:sbsize)]
        if (ijob == SID_SEQ_DIAG + 3) then
            call rci_op_subcopy(iS, task, m, lsize, &
                                MID_P, m, 0, (iter_diag-1)*sbsize, &
                                MID_PWORK1, m, 0, 2*lsize)
            ijob = ijob + 1
            return
        end if

        ! - PWORK2 = [HPsi(:,(it-1)*sbsize+1:sbsize)]
        if (ijob == SID_SEQ_DIAG + 4) then
            call rci_op_subcopy(iS, task, m, lsize, &
                                MID_HPsi, m, 0, (iter_diag-1)*sbsize, &
                                MID_PWORK2, m, 0, 0)
            ijob = ijob + 1
            return
        end if

        ! - PWORK2 = [PWORK2 HW(:,(it-1)*sbsize+1:sbsize)]
        if (ijob == SID_SEQ_DIAG + 5) then
            call rci_op_subcopy(iS, task, m, lsize, &
                                MID_HW, m, 0, (iter_diag-1)*sbsize, &
                                MID_PWORK2, m, 0, lsize)
            if (iter == 1) then
                ijob = ijob + 2
            else
                ijob = ijob + 1
            end if
            return
        end if

        ! - PWORK2 = [PWORK2 HP(:,(it-1)*sbsize+1:sbsize)]
        if (ijob == SID_SEQ_DIAG + 6) then
            call rci_op_subcopy(iS, task, m, lsize, &
                                MID_HP, m, 0, (iter_diag-1)*sbsize, &
                                MID_PWORK2, m, 0, 2*lsize)
            ijob = ijob + 1
            return
        end if

        ! - PWORK3 = [SPsi(:,(it-1)*sbsize+1:sbsize)]
        if (ijob == SID_SEQ_DIAG + 7) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_subcopy(iS, task, m, lsize, &
                                    MID_SPsi, m, 0, (iter_diag-1)*sbsize, &
                                    MID_PWORK3, m, 0, 0)
            end if
            ijob = ijob + 1
            return
        end if

        ! - PWORK3 = [PWORK3 SW(:,(it-1)*sbsize+1:sbsize)]
        if (ijob == SID_SEQ_DIAG + 8) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_subcopy(iS, task, m, lsize, &
                                    MID_SW, m, 0, (iter_diag-1)*sbsize, &
                                    MID_PWORK3, m, 0, lsize)
            end if
            if (iter == 1) then
                ijob = ijob + 2
            else
                ijob = ijob + 1
            end if
            return
        end if

        ! - PWORK3 = [PWORK3 SP(:,(it-1)*sbsize+1:sbsize)]
        if (ijob == SID_SEQ_DIAG + 9) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_subcopy(iS, task, m, lsize, &
                                    MID_SP, m, 0, (iter_diag-1)*sbsize, &
                                    MID_PWORK3, m, 0, 2*lsize)
            end if
            ijob = ijob + 1
            return
        end if

        ! - G = PWORK1'*PWORK2
        if (ijob == SID_SEQ_DIAG + 10) then
            call rci_op_gemm(iS, task, 'C', 'N', lsize3, lsize3, &
                             m, 1.0_r8, &
                             MID_PWORK1, m, MID_PWORK2, m, 0.0_r8, &
                             MID_G, max_n)
            ijob = ijob + 1
            return
        end if

        ! - WORK = PWORK1'*PWORK3
        if (ijob == SID_SEQ_DIAG + 11) then
            if (ovlp_is_unit) then
                call rci_op_gemm(iS, task, 'C', 'N', lsize3, lsize3, &
                                 m, 1.0_r8, &
                                 MID_PWORK1, m, MID_PWORK1, m, 0.0_r8, &
                                 MID_WORK, max_n)
            else
                call rci_op_gemm(iS, task, 'C', 'N', lsize3, lsize3, &
                                 m, 1.0_r8, &
                                 MID_PWORK1, m, MID_PWORK3, m, 0.0_r8, &
                                 MID_WORK, max_n)
            end if
            ijob = ijob + 1
            return
        end if

        ! - solve a local eigenvalue problem
        ! -- G * C = WORK * C * Diag(EW) eigenvector is in G
        if (ijob == SID_SEQ_DIAG + 12) then
            call rci_op_hegv(iS, task, 'V', 'L', lsize3, &
                         MID_G, max_n, MID_WORK, max_n)
            ijob = ijob + 1
            return
        end if

        ! - rotate Psi related matrices
        ! - PWORK*(Psi) = PWORK*(Psi)*G(1:lsize,1:lsize)
        if (ijob == SID_SEQ_DIAG + 13) then
            call rci_op_subcopy(iS, task, lsize, lsize, &
                                MID_G, max_n, 0, 0, &
                                MID_WORK, max_n, 0, 0)
            ijob = ijob + 1
            return
        end if
        ! - PWORK1(Psi) = PWORK1(Psi)*G(1:lsize,1:lsize)
        if (ijob == SID_SEQ_DIAG + 14) then
            call rci_op_subcopy(iS, task, m, lsize, &
                                MID_PWORK1, m, 0, 0, &
                                MID_PWORK0, m, 0, 0)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_SEQ_DIAG + 15) then
            call rci_op_gemm(iS, task, 'N', 'N', m, lsize, &
                             lsize, 1.0_r8, &
                             MID_PWORK0, m, MID_WORK, max_n, 0.0_r8, &
                             MID_PWORK, m)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_SEQ_DIAG + 16) then
            call rci_op_subcopy(iS, task, m, lsize, &
                                MID_PWORK, max_n, 0, 0, &
                                MID_PWORK1, max_n, 0, 0)
            ijob = ijob + 1
            return
        end if
        ! - PWORK2(Psi) = PWORK2(Psi)*G(1:lsize,1:lsize)
        if (ijob == SID_SEQ_DIAG + 17) then
            call rci_op_subcopy(iS, task, m, lsize, &
                                MID_PWORK2, m, 0, 0, &
                                MID_PWORK0, m, 0, 0)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_SEQ_DIAG + 18) then
            call rci_op_gemm(iS, task, 'N', 'N', m, lsize, &
                             lsize, 1.0_r8, &
                             MID_PWORK0, m, MID_WORK, max_n, 0.0_r8, &
                             MID_PWORK, m)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_SEQ_DIAG + 19) then
            call rci_op_subcopy(iS, task, m, lsize, &
                                MID_PWORK, max_n, 0, 0, &
                                MID_PWORK2, max_n, 0, 0)
            ijob = ijob + 1
            return
        end if
        ! - PWORK3(Psi) = PWORK3(Psi)*G(1:lsize,1:lsize)
        if (ijob == SID_SEQ_DIAG + 20) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_subcopy(iS, task, m, lsize, &
                                    MID_PWORK3, m, 0, 0, &
                                    MID_PWORK0, m, 0, 0)
            end if
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_SEQ_DIAG + 21) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_gemm(iS, task, 'N', 'N', m, lsize, &
                                 lsize, 1.0_r8, &
                                 MID_PWORK0, m, MID_WORK, max_n, 0.0_r8, &
                                 MID_PWORK, m)
            end if
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_SEQ_DIAG + 22) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_subcopy(iS, task, m, lsize, &
                                    MID_PWORK, max_n, 0, 0, &
                                    MID_PWORK3, max_n, 0, 0)
            end if
            ijob = ijob + 1
            return
        end if

        ! - rotate W related matrices
        ! - PWORK*(W) = PWORK*(W)*G(lsize+(1:lsize),1:lsize)
        if (ijob == SID_SEQ_DIAG + 23) then
            call rci_op_subcopy(iS, task, lsize, lsize, &
                                MID_G, max_n, lsize, 0, &
                                MID_WORK, max_n, 0, 0)
            ijob = ijob + 1
            return
        end if
        ! - PWORK1(W) = PWORK1(W)*G(lsize+(1:lsize),1:lsize)
        if (ijob == SID_SEQ_DIAG + 24) then
            call rci_op_subcopy(iS, task, m, lsize, &
                                MID_PWORK1, m, 0, lsize, &
                                MID_PWORK0, m, 0, 0)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_SEQ_DIAG + 25) then
            call rci_op_gemm(iS, task, 'N', 'N', m, lsize, &
                             lsize, 1.0_r8, &
                             MID_PWORK0, m, MID_WORK, max_n, 0.0_r8, &
                             MID_PWORK, m)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_SEQ_DIAG + 26) then
            call rci_op_subcopy(iS, task, m, lsize, &
                                MID_PWORK, max_n, 0, 0, &
                                MID_PWORK1, max_n, 0, lsize)
            ijob = ijob + 1
            return
        end if
        ! - PWORK2(W) = PWORK2(W)*G(lsize+(1:lsize),1:lsize)
        if (ijob == SID_SEQ_DIAG + 27) then
            call rci_op_subcopy(iS, task, m, lsize, &
                                MID_PWORK2, m, 0, lsize, &
                                MID_PWORK0, m, 0, 0)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_SEQ_DIAG + 28) then
            call rci_op_gemm(iS, task, 'N', 'N', m, lsize, &
                             lsize, 1.0_r8, &
                             MID_PWORK0, m, MID_WORK, max_n, 0.0_r8, &
                             MID_PWORK, m)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_SEQ_DIAG + 29) then
            call rci_op_subcopy(iS, task, m, lsize, &
                                MID_PWORK, max_n, 0, 0, &
                                MID_PWORK2, max_n, 0, lsize)
            ijob = ijob + 1
            return
        end if
        ! - PWORK3(W) = PWORK3(W)*G(lsize+(1:lsize),1:lsize)
        if (ijob == SID_SEQ_DIAG + 30) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_subcopy(iS, task, m, lsize, &
                                    MID_PWORK3, m, 0, lsize, &
                                    MID_PWORK0, m, 0, 0)
            end if
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_SEQ_DIAG + 31) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_gemm(iS, task, 'N', 'N', m, lsize, &
                                 lsize, 1.0_r8, &
                                 MID_PWORK0, m, MID_WORK, max_n, 0.0_r8, &
                                 MID_PWORK, m)
            end if
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_SEQ_DIAG + 32) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_subcopy(iS, task, m, lsize, &
                                    MID_PWORK, max_n, 0, 0, &
                                    MID_PWORK3, max_n, 0, lsize)
            end if
            if (iter == 1) then
                ijob = ijob + 11
            else
                ijob = ijob + 1
            end if
            return
        end if

        ! - rotate P related matrices
        ! - PWORK*(P) = PWORK*(P)*G(2*lsize+(1:lsize),1:lsize)
        if (ijob == SID_SEQ_DIAG + 33) then
            call rci_op_subcopy(iS, task, lsize, lsize, &
                                MID_G, max_n, 2*lsize, 0, &
                                MID_WORK, max_n, 0, 0)
            ijob = ijob + 1
            return
        end if
        ! - PWORK1(P) = PWORK1(P)*G(2*lsize+(1:lsize),1:lsize)
        if (ijob == SID_SEQ_DIAG + 34) then
            call rci_op_subcopy(iS, task, m, lsize, &
                                MID_PWORK1, m, 0, 2*lsize, &
                                MID_PWORK0, m, 0, 0)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_SEQ_DIAG + 35) then
            call rci_op_gemm(iS, task, 'N', 'N', m, lsize, &
                             lsize, 1.0_r8, &
                             MID_PWORK0, m, MID_WORK, max_n, 0.0_r8, &
                             MID_PWORK, m)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_SEQ_DIAG + 36) then
            call rci_op_subcopy(iS, task, m, lsize, &
                                MID_PWORK, max_n, 0, 0, &
                                MID_PWORK1, max_n, 0, 2*lsize)
            ijob = ijob + 1
            return
        end if
        ! - PWORK2(P) = PWORK2(P)*G(2*lsize+(1:lsize),1:lsize)
        if (ijob == SID_SEQ_DIAG + 37) then
            call rci_op_subcopy(iS, task, m, lsize, &
                                MID_PWORK2, m, 0, 2*lsize, &
                                MID_PWORK0, m, 0, 0)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_SEQ_DIAG + 38) then
            call rci_op_gemm(iS, task, 'N', 'N', m, lsize, &
                             lsize, 1.0_r8, &
                             MID_PWORK0, m, MID_WORK, max_n, 0.0_r8, &
                             MID_PWORK, m)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_SEQ_DIAG + 39) then
            call rci_op_subcopy(iS, task, m, lsize, &
                                MID_PWORK, max_n, 0, 0, &
                                MID_PWORK2, max_n, 0, 2*lsize)
            ijob = ijob + 1
            return
        end if
        ! - PWORK3(P) = PWORK3(P)*G(2*lsize+(1:lsize),1:lsize)
        if (ijob == SID_SEQ_DIAG + 40) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_subcopy(iS, task, m, lsize, &
                                    MID_PWORK3, m, 0, 2*lsize, &
                                    MID_PWORK0, m, 0, 0)
            end if
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_SEQ_DIAG + 41) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_gemm(iS, task, 'N', 'N', m, lsize, &
                                 lsize, 1.0_r8, &
                                 MID_PWORK0, m, MID_WORK, max_n, 0.0_r8, &
                                 MID_PWORK, m)
            end if
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_SEQ_DIAG + 42) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_subcopy(iS, task, m, lsize, &
                                    MID_PWORK, max_n, 0, 0, &
                                    MID_PWORK3, max_n, 0, 2*lsize)
            end if
            ijob = ijob + 1
            return
        end if

        ! - P = WG+PG
        ! - PWORK = WG(PWORK1)
        if (ijob == SID_SEQ_DIAG + 43) then
            call rci_op_subcopy(iS, task, m, lsize, &
                                MID_PWORK1, m, 0, lsize, &
                                MID_PWORK, m, 0, 0)
            if (iter == 1) then
                ijob = ijob + 3
            else
                ijob = ijob + 1
            end if
            return
        end if
        ! - PWORK0 = PG(PWORK1)
        if (ijob == SID_SEQ_DIAG + 44) then
            call rci_op_subcopy(iS, task, m, lsize, &
                                MID_PWORK1, m, 0, 2*lsize, &
                                MID_PWORK0, m, 0, 0)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_SEQ_DIAG + 45) then
            call rci_op_axpy(iS, task, m, lsize, 1.0_r8, &
                             MID_PWORK0, m, MID_PWORK, m)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_SEQ_DIAG + 46) then
            call rci_op_subcopy(iS, task, m, lsize, &
                                MID_PWORK, m, 0, 0, &
                                MID_P, m, 0, (iter_diag-1)*sbsize)
            ijob = ijob + 1
            return
        end if

        ! - Psi = Psi + P
        if (ijob == SID_SEQ_DIAG + 47) then
            call rci_op_axpy(iS, task, m, lsize, 1.0_r8, &
                             MID_PWORK1, m, MID_PWORK, m)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_SEQ_DIAG + 48) then
            call rci_op_subcopy(iS, task, m, lsize, &
                                MID_PWORK, m, 0, 0, &
                                MID_Psi, m, 0, (iter_diag-1)*sbsize)
            ijob = ijob + 1
            return
        end if

        ! - HP = HWG+HPG
        ! - PWORK = HWG(PWORK2)
        if (ijob == SID_SEQ_DIAG + 49) then
            call rci_op_subcopy(iS, task, m, lsize, &
                                MID_PWORK2, m, 0, lsize, &
                                MID_PWORK, m, 0, 0)
            if (iter == 1) then
                ijob = ijob + 3
            else
                ijob = ijob + 1
            end if
            return
        end if
        ! - PWORK0 = HPG(PWORK2)
        if (ijob == SID_SEQ_DIAG + 50) then
            call rci_op_subcopy(iS, task, m, lsize, &
                                MID_PWORK2, m, 0, 2*lsize, &
                                MID_PWORK0, m, 0, 0)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_SEQ_DIAG + 51) then
            call rci_op_axpy(iS, task, m, lsize, 1.0_r8, &
                             MID_PWORK0, m, MID_PWORK, m)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_SEQ_DIAG + 52) then
            call rci_op_subcopy(iS, task, m, lsize, &
                                MID_PWORK, m, 0, 0, &
                                MID_HP, m, 0, (iter_diag-1)*sbsize)
            ijob = ijob + 1
            return
        end if

        ! - HPsi = HPsi + HP
        if (ijob == SID_SEQ_DIAG + 53) then
            call rci_op_axpy(iS, task, m, lsize, 1.0_r8, &
                             MID_PWORK2, m, MID_PWORK, m)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_SEQ_DIAG + 54) then
            call rci_op_subcopy(iS, task, m, lsize, &
                                MID_PWORK, m, 0, 0, &
                                MID_HPsi, m, 0, (iter_diag-1)*sbsize)
            ijob = ijob + 1
            return
        end if

        ! - SP = SWG+SPG
        ! - PWORK = SWG(PWORK3)
        if (ijob == SID_SEQ_DIAG + 55) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_subcopy(iS, task, m, lsize, &
                                    MID_PWORK3, m, 0, lsize, &
                                    MID_PWORK, m, 0, 0)
            end if
            if (iter == 1) then
                ijob = ijob + 3
            else
                ijob = ijob + 1
            end if
            return
        end if
        ! - PWORK0 = SPG(PWORK3)
        if (ijob == SID_SEQ_DIAG + 56) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_subcopy(iS, task, m, lsize, &
                                    MID_PWORK3, m, 0, 2*lsize, &
                                    MID_PWORK0, m, 0, 0)
            end if
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_SEQ_DIAG + 57) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_axpy(iS, task, m, lsize, 1.0_r8, &
                                 MID_PWORK0, m, MID_PWORK, m)
            end if
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_SEQ_DIAG + 58) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_subcopy(iS, task, m, lsize, &
                                    MID_PWORK, m, 0, 0, &
                                    MID_SP, m, 0, (iter_diag-1)*sbsize)
            end if
            ijob = ijob + 1
            return
        end if

        ! - SPsi = SPsi + SP
        if (ijob == SID_SEQ_DIAG + 59) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_axpy(iS, task, m, lsize, 1.0_r8, &
                                 MID_PWORK3, m, MID_PWORK, m)
            end if
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_SEQ_DIAG + 60) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_subcopy(iS, task, m, lsize, &
                                    MID_PWORK, m, 0, 0, &
                                    MID_SPsi, m, 0, (iter_diag-1)*sbsize)
            end if
            if (iter_diag == nsb) then
                ijob = SID_ORTH
            else
                ijob = SID_SEQ_DIAG
            end if
            return
        end if

        ! - G = Psi'*SPsi
        if (ijob == SID_ORTH) then
            call rci_op_copy(iS, task, 'N', MID_Psi, MID_PWORK)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_ORTH + 1) then
            if (ovlp_is_unit) then
                call rci_op_gemm(iS, task, 'C', 'N', nact, nact, &
                                 m, 1.0_r8, &
                                 MID_PWORK, m, MID_Psi, m, 0.0_r8, &
                                 MID_G, max_n)
            else
                call rci_op_gemm(iS, task, 'C', 'N', nact, nact, &
                                 m, 1.0_r8, &
                                 MID_PWORK, m, MID_SPsi, m, 0.0_r8, &
                                 MID_G, max_n)
            end if
            ijob = ijob + 1
            return
        end if

        ! - Cholesky factorization of G
        if (ijob == SID_ORTH + 2) then
            call rci_op_potrf(iS, task, 'U', nact, MID_G, max_n)
            ijob = ijob + 1
            return
        end if

        ! - Psi = Psi/G
        if (ijob == SID_ORTH + 3) then
            call rci_op_trsm(iS, task, 'R', 'U', 'N', m, nact, 1.0_r8, &
                             MID_G, max_n, MID_Psi, m)
            ijob = ijob + 1
            return
        end if

        ! - HPsi = HPsi/G
        if (ijob == SID_ORTH + 4) then
            call rci_op_trsm(iS, task, 'R', 'U', 'N', m, nact, 1.0_r8, &
                             MID_G, max_n, MID_HPsi, m)
            ijob = ijob + 1
            return
        end if

        ! - SPsi = SPsi/G
        if (ijob == SID_ORTH + 5) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_trsm(iS, task, 'R', 'U', 'N', m, nact, 1.0_r8, &
                                 MID_G, max_n, MID_SPsi, m)
            end if
            ijob = SID_RR
            return
        end if

        ! Determine whether to do RR
        if (ijob == SID_RR) then
            if ((mod(iter, rrstep) == 0) &
                .or. (iter == max_iter) &
                .or. (conv_flag)) then
                ijob = ijob + 1
            else
                ijob = SID_NORR
            end if
            call rci_op_null(task)
            return
        end if

        ! - PWORK1 = [Psi_lock Psi]
        if (ijob == SID_RR + 1) then
            call rci_op_subcopy(iS, task, m, n-nact, &
                                MID_Psi_lock, m, 0, 0, &
                                MID_PWORK1, m, 0, 0)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_RR + 2) then
            call rci_op_subcopy(iS, task, m, nact, &
                                MID_Psi, m, 0, 0, &
                                MID_PWORK1, m, 0, n-nact)
            ijob = ijob + 1
            return
        end if

        ! - PWORK2 = [HPsi_lock HPsi]
        if (ijob == SID_RR + 3) then
            call rci_op_subcopy(iS, task, m, n-nact, &
                                MID_HPsi_lock, m, 0, 0, &
                                MID_PWORK2, m, 0, 0)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_RR + 4) then
            call rci_op_subcopy(iS, task, m, nact, &
                                MID_HPsi, m, 0, 0, &
                                MID_PWORK2, m, 0, n-nact)
            ijob = ijob + 1
            return
        end if

        ! - PWORK3 = [SPsi_lock SPsi]
        if (ijob == SID_RR + 5) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_subcopy(iS, task, m, n-nact, &
                                    MID_SPsi_lock, m, 0, 0, &
                                    MID_PWORK3, m, 0, 0)
            end if
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_RR + 6) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_subcopy(iS, task, m, nact, &
                                    MID_SPsi, m, 0, 0, &
                                    MID_PWORK3, m, 0, n-nact)
            end if
            ijob = ijob + 1
            return
        end if

        ! - G = PWORK1'*PWORK2
        if (ijob == SID_RR + 7) then
            call rci_op_gemm(iS, task, 'C', 'N', n, n, &
                             m, 1.0_r8, &
                             MID_PWORK1, m, MID_PWORK2, m, 0.0_r8, &
                             MID_G, max_n)
            ijob = ijob + 1
            return
        end if

        ! - WORK = PWORK1'*PWORK3
        if (ijob == SID_RR + 8) then
            if (ovlp_is_unit) then
                call rci_op_gemm(iS, task, 'C', 'N', n, n, &
                                 m, 1.0_r8, &
                                 MID_PWORK1, m, MID_PWORK1, m, 0.0_r8, &
                                 MID_WORK, max_n)
            else
                call rci_op_gemm(iS, task, 'C', 'N', n, n, &
                                 m, 1.0_r8, &
                                 MID_PWORK1, m, MID_PWORK3, m, 0.0_r8, &
                                 MID_WORK, max_n)
            end if
            ijob = ijob + 1
            return
        end if

        ! - solve a RR eigenvalue problem
        ! -- G * C = WORK * C * Diag(EW) eigenvector is in G
        if (ijob == SID_RR + 9) then
            call rci_op_hegv(iS, task, 'V', 'L', n, &
                         MID_G, max_n, MID_WORK, max_n)
            ijob = ijob + 1
            return
        end if

        ! - Psi = Psi*G
        if (ijob == SID_RR + 10) then
            r_h%total_energy = sum(resvec(1:n))
            call rci_op_gemm(iS, task, 'N', 'N', m, n, &
                             n, 1.0_r8, &
                             MID_PWORK1, m, MID_G, max_n, 0.0_r8, &
                             MID_Psi, m)
            ijob = ijob + 1
            return
        end if

        ! - HPsi = HPsi*G
        if (ijob == SID_RR + 11) then
            call rci_op_gemm(iS, task, 'N', 'N', m, n, &
                             n, 1.0_r8, &
                             MID_PWORK2, m, MID_G, max_n, 0.0_r8, &
                             MID_HPsi, m)
            ijob = ijob + 1
            return
        end if

        ! - SPsi = SPsi*G
        if (ijob == SID_RR + 12) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_gemm(iS, task, 'N', 'N', m, n, &
                                 n, 1.0_r8, &
                                 MID_PWORK3, m, MID_G, max_n, 0.0_r8, &
                                 MID_SPsi, m)
            end if
            ijob = ijob + 1
            return
        end if

        ! - PWORK = SPsi*Diag(resvec)
        if (ijob == SID_RR + 13) then
            if (ovlp_is_unit) then
                call rci_op_copy(iS, task, 'N', MID_Psi, MID_PWORK)
            else
                call rci_op_copy(iS, task, 'N', MID_SPsi, MID_PWORK)
            end if
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_RR + 14) then
            call rci_op_colscale(iS, task, m, n, MID_PWORK, m)
            ijob = ijob + 1
            return
        end if

        ! - W = HPsi - PWORK
        if (ijob == SID_RR + 15) then
            call rci_op_copy(iS, task, 'N', MID_HPsi, MID_W)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_RR + 16) then
            call rci_op_axpy(iS, task, m, n, -1.0_r8, &
                             MID_PWORK, m, MID_W, m)
            if (conv_flag) then
                ijob = SID_FINISH + 1
            else
                ijob = SID_LOCK
            end if
            return
        end if


        ! NO RR
        ! - G = Psi'*HPsi
        if (ijob == SID_NORR) then
            call rci_op_gemm(iS, task, 'C', 'N', nact, nact, &
                             m, 1.0_r8, &
                             MID_Psi, m, MID_HPsi, m, 0.0_r8, &
                             MID_G, max_n)
            ijob = ijob + 1
            return
        end if

        ! - W = HPsi - SPsi*G
        if (ijob == SID_NORR + 1) then
            call rci_op_copy(iS, task, 'N', MID_HPsi, MID_W)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_NORR + 2) then
            if (ovlp_is_unit) then
                call rci_op_gemm(iS, task, 'N', 'N', m, nact, &
                                 nact, -1.0_r8, &
                                 MID_Psi, m, MID_G, max_n, 1.0_r8, &
                                 MID_W, m)
            else
                call rci_op_gemm(iS, task, 'N', 'N', m, nact, &
                                 nact, -1.0_r8, &
                                 MID_SPsi, m, MID_G, max_n, 1.0_r8, &
                                 MID_W, m)
            end if
            ijob = ijob + 1
            return
        end if

        ! - trG = trace(G)
        if (ijob == SID_NORR + 3) then
            call rci_op_trace(iS, task, nact, MID_G, max_n)
            ijob = ijob + 1
            return
        end if

        ! - Check conv
        if (ijob == SID_NORR + 4) then
            trG = resvec(1)
            trdiff = abs((trG-trG_old)/trG)

            if (r_h%verbose > 0) write(*,'(a,i5,a,es15.7e3)') &
                'Iter ', iter, ', Rel Err: ', trdiff

            if ((trdiff < tol_iter) &
                .or. (iter == max_iter)) then
                call rci_op_null(task)
                ijob = SID_FINISH
                conv_flag = .true.
                return
            end if
            iter = iter + 1

            call rci_op_null(task)
            ijob = SID_ITER
            return
        end if

        ! - Psi = [Psi_lock Psi]
        if (ijob == SID_FINISH) then
            if (iter == max_iter) then
                ijob = ijob + 1
            else
                ijob = SID_RR
            end if
            call rci_op_null(task)
            return
        end if

        if (ijob == SID_FINISH + 1) then
            call rci_op_copy(iS, task, 'N', MID_PWORK1, MID_Psi)
            ijob = ijob + 1
            return
        end if

        if (ijob == SID_FINISH + 2) then
            r_h%total_iter = iter
            if (r_h%verbose > 0) then
                write(*,*) 'Eigenvalues are:'
                print *, resvec(1:n)
            end if
            call rci_op_converge(task, trdiff < tol_iter)
            return
        end if

    end subroutine

end module ELSI_RCI_PPCG
