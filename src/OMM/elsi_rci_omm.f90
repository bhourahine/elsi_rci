! Copyright (c) 2015-2019, the ELSI team.
! All rights reserved.
!
! This file is part of ELSI and is distributed under the BSD 3-clause license,
! which may be found in the LICENSE file in the ELSI root directory.

!>
!! This module contains subroutines to solve an eigenproblem using a reverse
!! communication interface.
!!
module ELSI_RCI_OMM

    use ELSI_RCI_OPS
    use ELSI_RCI_CONSTANTS
    use ELSI_RCI_DATATYPE
    use ELSI_RCI_PRECISION, only:i4

    implicit none

    public :: rci_omm
    public :: rci_omm_allocate
    public :: rci_omm_deallocate

    ! Matrix ID of size m by n
    !&<
    integer(i4), parameter :: MID_C    = 1
    integer(i4), parameter :: MID_HC   = 2
    integer(i4), parameter :: MID_G    = 3
    integer(i4), parameter :: MID_PG   = 4
    integer(i4), parameter :: MID_D    = 5
    integer(i4), parameter :: MID_HD   = 6
    integer(i4), parameter :: MID_Gp   = 7
    integer(i4), parameter :: MID_PGp  = 8
    integer(i4), parameter :: MID_WORK = 9
    integer(i4), parameter :: MID_SC   = 10
    integer(i4), parameter :: MID_SD   = 11
    !&>

    ! Matrix ID of size n by n
    !&<
    integer(i4), parameter :: MID_HW   = 21
    integer(i4), parameter :: MID_SW   = 22
    integer(i4), parameter :: MID_HWd  = 23
    integer(i4), parameter :: MID_SWd  = 24
    integer(i4), parameter :: MID_HWdd = 25
    integer(i4), parameter :: MID_SWdd = 26
    integer(i4), parameter :: MID_HWdT = 27
    integer(i4), parameter :: MID_SWdT = 28
    !&>

    ! Stage ID
    !&<
    integer(i4), parameter :: SID_INIT        = 0
    integer(i4), parameter :: SID_COEFF       = 100
    integer(i4), parameter :: SID_ITER        = 200
    integer(i4), parameter :: SID_UPDATE      = 300
    integer(i4), parameter :: SID_LS_FAIL     = 400
    integer(i4), parameter :: SID_LS_CONV     = 500
    integer(i4), parameter :: SID_FINISH      = 600
    integer(i4), parameter :: SID_ALLOCATE    = 700
    integer(i4), parameter :: SID_DEALLOCATE  = 800
    !&>

contains

    subroutine rci_omm_allocate(r_h, ijob, iS, task)

        implicit none

        !**** INPUT ***********************************!
        type(rci_handle), intent(in) :: r_h

        !**** OUTPUT **********************************!
        integer(i4), intent(out) :: task

        !**** INOUT ***********************************!
        integer(i4), intent(inout) :: ijob ! job id
        type(rci_instr), intent(inout) :: iS

        !**** LOCAL ***********************************!
        integer(i4), save :: iter ! CG step num.
        integer(i4), save :: m, n ! size of the wave functions.

        if (ijob <= SID_ALLOCATE) then
            ijob = SID_ALLOCATE + 1
            m = r_h%n_basis
            n = r_h%max_n
            iter = 0
        end if

        ! Allocate matrix of size m by n
        if (ijob == SID_ALLOCATE + 1) then
            iter = iter + 1
            if (iter > 11) then
                ijob = ijob + 1
                iter = 20
                call rci_op_null(task)
            elseif ((iter > 9) .and. (r_h%ovlp_is_unit)) then
                ijob = ijob + 1
                iter = 20
                call rci_op_null(task)
            else
                call rci_op_allocate(iS, task, m, n, iter)
            end if
            return
        end if

        ! Allocate matrix of size n by n
        if (ijob == SID_ALLOCATE + 2) then
            iter = iter + 1
            if (iter > 28) then
                call rci_op_stop(task)
            else
                call rci_op_allocate(iS, task, n, n, iter)
            end if
            return
        end if

    end subroutine

    subroutine rci_omm_deallocate(r_h, ijob, iS, task)

        implicit none

        !**** INPUT ***********************************!
        type(rci_handle), intent(in) :: r_h

        !**** OUTPUT **********************************!
        integer(i4), intent(out) :: task

        !**** INOUT ***********************************!
        integer(i4), intent(inout) :: ijob ! job id
        type(rci_instr), intent(inout) :: iS

        !**** LOCAL ***********************************!
        integer(i4), save :: iter ! CG step num.

        if (ijob <= SID_DEALLOCATE) then
            ijob = SID_DEALLOCATE + 1
            iter = 0
        end if

        ! Deallocate matrix of size m by n
        if (ijob == SID_DEALLOCATE + 1) then
            iter = iter + 1
            if (iter > 11) then
                ijob = ijob + 1
                iter = 20
                call rci_op_null(task)
            elseif ((iter > 9) .and. (r_h%ovlp_is_unit)) then
                ijob = ijob + 1
                iter = 20
                call rci_op_null(task)
            else
                call rci_op_deallocate(iS, task, iter)
            end if
            return
        end if

        ! Deallocate matrix of size n by n
        if (ijob == SID_DEALLOCATE + 2) then
            iter = iter + 1
            if (iter > 28) then
                call rci_op_stop(task)
            else
                call rci_op_deallocate(iS, task, iter)
            end if
            return
        end if

    end subroutine

    subroutine rci_omm(r_h, ijob, iS, task, resvec)

        implicit none

        !**** OUTPUT **********************************!
        integer(i4), intent(out) :: task

        !**** INOUT ***********************************!
        type(rci_handle), intent(inout) :: r_h
        integer(i4), intent(inout) :: ijob ! job id
        type(rci_instr), intent(inout) :: iS
        real(r8), intent(inout) :: resvec(:)

        !**** LOCAL ***********************************!
        integer(i4), save :: m ! size of basis
        integer(i4), save :: n ! number of occupied states
        integer(i4), save :: max_iter
        integer(i4), save :: max_n ! max number of columns
        real(r8), save :: tol_iter ! convergence tolerance of CG

        logical, save :: ls_conv ! line search converged?
        logical, save :: ls_fail ! line search failed?

        integer(i4), save :: icg ! CG step num.

        real(r8), save :: est_ub ! estimated upper bound of spectrum
        real(r8), save :: lambda ! CG step size
        real(r8), save :: lambda_d ! lambda denominator
        real(r8), save :: lambda_n ! lambda numerator
        real(r8), save :: e_min
        real(r8), save :: e_min_old ! OMM func energy at previous CG step
        real(r8), save :: coeff(0:4) ! coeffs. of the quartic equation
        real(r8), save :: x_min ! line search position of minimum
        real(r8), save :: e_diff

        real(r8), save :: TrH
        real(r8), save :: TrHS
        real(r8), save :: TrHd
        real(r8), save :: TrHdS
        real(r8), save :: TrHSd
        real(r8), save :: TrHdd
        real(r8), save :: TrHddS
        real(r8), save :: TrHSdd
        real(r8), save :: TrHdSd
        real(r8), save :: TrHdSdT
        real(r8), save :: TrHddSd
        real(r8), save :: TrHdSdd
        real(r8), save :: TrHddSdd

        logical, save :: ovlp_is_unit ! ovlp is unit flag
        logical, save :: conv
        !**********************************************!

        ! if this is the first SCF step, then we need to initialize the WF
        ! coeffs. matrix with random numbers between -0.5 and 0.5 (normalize
        ! at the end to avoid instabilities), unless we are reading them
        ! from file

        ! first we calculate the energy and gradient for our initial guess,
        ! with the following steps:
        ! -calculate the hamiltonian in WF basis: HW=C^T*H*C
        ! -- HC = H*C
        if (ijob <= SID_INIT) then
            ijob = SID_INIT + 1
            tol_iter = r_h%tol_iter
            max_iter = r_h%max_iter
            m = r_h%n_basis
            n = r_h%n_state
            max_n = r_h%max_n
            ovlp_is_unit = r_h%ovlp_is_unit
            est_ub = r_h%omm_est_ub
            icg = 0
            lambda = 0.0_r8
            call rci_op_null(task)
            return
        end if

        if (ijob <= SID_INIT + 1) then
            call rci_op_h_multi(iS, task, 'N', m, n, MID_C, MID_HC)
            ijob = ijob + 1
            return
        end if

        ! -calculate the overlap matrix in WF basis: SW=C^T*S*C
        ! -- SC = S*C
        if (ijob == SID_INIT + 2) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_s_multi(iS, task, 'N', m, n, MID_C, MID_SC)
            end if
            ijob = ijob + 1
            return
        end if

        if (ijob == SID_INIT + 3) then
            if (ovlp_is_unit) then
                call rci_op_axpy(iS, task, m, n, -est_ub, MID_C, m, MID_HC, m)
            else
                call rci_op_axpy(iS, task, m, n, -est_ub, MID_SC, m, MID_HC, m)
            end if
            ijob = ijob + 1
            return
        end if

        ! -- HW = C'*HC
        if (ijob == SID_INIT + 4) then
            call rci_op_gemm(iS, task, 'C', 'N', n, n, m, 1.0_r8, &
                             MID_C, m, MID_HC, m, 0.0_r8, MID_HW, max_n)
            ijob = ijob + 1
            return
        end if

        ! -- SW = C'*SC
        if (ijob == SID_INIT + 5) then
            if (ovlp_is_unit) then
                call rci_op_gemm(iS, task, 'C', 'N', n, n, m, 1.0_r8, &
                                 MID_C, m, MID_C, m, 0.0_r8, MID_SW, max_n)
            else
                call rci_op_gemm(iS, task, 'C', 'N', n, n, m, 1.0_r8, &
                                 MID_C, m, MID_SC, m, 0.0_r8, MID_SW, max_n)
            end if
            ijob = ijob + 1
            return
        end if

        ! -calculate the gradient: G=2*(2*HC-SC*HW-HC*SW)
        ! Note: It is required that G = 0 as the initial
        ! -- G = 4*HC
        if (ijob == SID_INIT + 6) then
            call rci_op_axpy(iS, task, m, n, 4.0_r8, MID_HC, m, MID_G, m)
            ijob = ijob + 1
            return
        end if
        ! -- G = -2 HC*SW + G
        if (ijob == SID_INIT + 7) then
            call rci_op_gemm(iS, task, 'N', 'N', m, n, n, -2.0_r8, &
                             MID_HC, m, MID_SW, max_n, 1.0_r8, MID_G, m)
            ijob = ijob + 1
            return
        end if
        ! -- G = -2 SC*HW + G
        if (ijob == SID_INIT + 8) then
            if (ovlp_is_unit) then
                call rci_op_gemm(iS, task, 'N', 'N', m, n, n, -2.0_r8, &
                                 MID_C, m, MID_HW, max_n, 1.0_r8, MID_G, m)
            else
                call rci_op_gemm(iS, task, 'N', 'N', m, n, n, -2.0_r8, &
                                 MID_SC, m, MID_HW, max_n, 1.0_r8, MID_G, m)
            end if
            ijob = ijob + 1
            return
        end if

        ! -calculate the preconditioned gradient by premultiplying G by P
        ! -- PG = P*G
        if (ijob == SID_INIT + 9) then
            call rci_op_p_multi(iS, task, 'N', m, n, MID_G, MID_PG)
            ijob = ijob + 1
            return
        end if

        ! - calculate HWd = G'*H*C = PG'*HC
        if (ijob == SID_INIT + 10) then
            call rci_op_gemm(iS, task, 'C', 'N', n, n, m, 1.0_r8, &
                             MID_PG, m, MID_HC, m, 0.0_r8, MID_HWd, max_n)
            ijob = ijob + 1
            return
        end if

        ! - calculate SWd = G'*S*C = PG'*SC
        if (ijob == SID_INIT + 11) then
            if (ovlp_is_unit) then
                call rci_op_gemm(iS, task, 'C', 'N', n, n, m, 1.0_r8, &
                                 MID_PG, m, MID_C, m, 0.0_r8, MID_SWd, max_n)
            else
                call rci_op_gemm(iS, task, 'C', 'N', n, n, m, 1.0_r8, &
                                 MID_PG, m, MID_SC, m, 0.0_r8, MID_SWd, max_n)
            end if
            ijob = ijob + 1
            return
        end if

        ! - calculate HWdd = G'*H*G = PG'*(H*PG)
        ! -- HD = H*PG
        if (ijob == SID_INIT + 12) then
            call rci_op_h_multi(iS, task, 'N', m, n, MID_PG, MID_HD)
            ijob = ijob + 1
            return
        end if
        ! -- SD = S*PG
        if (ijob == SID_INIT + 13) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_s_multi(iS, task, 'N', m, n, MID_PG, MID_SD)
            end if
            ijob = ijob + 1
            return
        end if

        if (ijob == SID_INIT + 14) then
            if (ovlp_is_unit) then
                call rci_op_axpy(iS, task, m, n, -est_ub, MID_PG, m, MID_HD, m)
            else
                call rci_op_axpy(iS, task, m, n, -est_ub, MID_SD, m, MID_HD, m)
            end if
            ijob = ijob + 1
            return
        end if

        ! -- HWdd = PG'*HD
        if (ijob == SID_INIT + 15) then
            call rci_op_gemm(iS, task, 'C', 'N', n, n, m, 1.0_r8, &
                             MID_PG, m, MID_HD, m, 0.0_r8, &
                             MID_HWdd, max_n)
            ijob = ijob + 1
            return
        end if

        !  SWdd=G^T*S*G
        ! - calculate SWdd = G'*S*G = PG'*(S*PG)
        ! -- SWdd = PG'*SD
        if (ijob == SID_INIT + 16) then
            if (ovlp_is_unit) then
                call rci_op_gemm(iS, task, 'C', 'N', n, n, m, 1.0_r8, &
                                 MID_PG, m, MID_PG, m, 0.0_r8, &
                                 MID_SWdd, max_n)
            else
                call rci_op_gemm(iS, task, 'C', 'N', n, n, m, 1.0_r8, &
                                 MID_PG, m, MID_SD, m, 0.0_r8, &
                                 MID_SWdd, max_n)
            end if
            ijob = SID_COEFF
            return
        end if

        ! calculate coefficients
        ! - TrH = trace(HW)
        if (ijob == SID_COEFF) then
            call rci_op_trace(iS, task, n, MID_HW, max_n)
            ijob = ijob + 1
            return
        end if
        ! - TrHS = trace(HW*SW)
        if (ijob == SID_COEFF + 1) then
            TrH = resvec(1)
            call rci_op_dot(iS, task, n, n, MID_HW, max_n, MID_SW, max_n)
            ijob = ijob + 1
            return
        end if
        ! - TrHd = trace(HWd)
        if (ijob == SID_COEFF + 2) then
            TrHS = resvec(1)
            call rci_op_trace(iS, task, n, MID_HWd, max_n)
            ijob = ijob + 1
            return
        end if
        ! - TrHdS = trace(HWd*SW)
        if (ijob == SID_COEFF + 3) then
            TrHd = resvec(1)
            call rci_op_dot(iS, task, n, n, MID_HWd, max_n, MID_SW, max_n)
            ijob = ijob + 1
            return
        end if
        ! - TrHSd = trace(HW*SWd)
        if (ijob == SID_COEFF + 4) then
            TrHdS = resvec(1)
            call rci_op_dot(iS, task, n, n, MID_HW, max_n, MID_SWd, max_n)
            ijob = ijob + 1
            return
        end if
        ! - TrHdd = trace(HWdd)
        if (ijob == SID_COEFF + 5) then
            TrHSd = resvec(1)
            call rci_op_trace(iS, task, n, MID_HWdd, max_n)
            ijob = ijob + 1
            return
        end if
        ! - TrHddS = trace(HWdd*SW)
        if (ijob == SID_COEFF + 6) then
            TrHdd = resvec(1)
            call rci_op_dot(iS, task, n, n, MID_HWdd, max_n, &
                            MID_SW, max_n)
            ijob = ijob + 1
            return
        end if
        ! - TrHSdd = trace(HW*SWdd)
        if (ijob == SID_COEFF + 7) then
            TrHddS = resvec(1)
            call rci_op_dot(iS, task, n, n, MID_HW, max_n, &
                            MID_SWdd, max_n)
            ijob = ijob + 1
            return
        end if
        ! - TrHdSdT = trace(HWd*SWdT)
        ! - SWdT = SWd'
        if (ijob == SID_COEFF + 8) then
            TrHSdd = resvec(1)
            call rci_op_copy(iS, task, 'C', MID_SWd, MID_SWdT)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_COEFF + 9) then
            call rci_op_dot(iS, task, n, n, MID_HWd, max_n, &
                            MID_SWdT, max_n)
            ijob = ijob + 1
            return
        end if
        ! - TrHdSd = trace(HWd*SWd)
        if (ijob == SID_COEFF + 10) then
            TrHdSdT = resvec(1)
            call rci_op_dot(iS, task, n, n, MID_HWd, max_n, &
                            MID_SWd, max_n)
            ijob = ijob + 1
            return
        end if
        ! - TrHddSd = trace(HWdd*SWd)
        if (ijob == SID_COEFF + 11) then
            TrHdSd = resvec(1)
            call rci_op_dot(iS, task, n, n, MID_HWdd, max_n, &
                            MID_SWd, max_n)
            ijob = ijob + 1
            return
        end if
        ! - TrHdSdd = trace(HWd*SWdd)
        if (ijob == SID_COEFF + 12) then
            TrHddSd = resvec(1)
            call rci_op_dot(iS, task, n, n, MID_HWd, max_n, &
                            MID_SWdd, max_n)
            ijob = ijob + 1
            return
        end if
        ! - TrHddSdd = trace(HWdd*SWdd)
        if (ijob == SID_COEFF + 13) then
            TrHdSdd = resvec(1)
            call rci_op_dot(iS, task, n, n, MID_HWdd, max_n, &
                            MID_SWdd, max_n)
            ijob = ijob + 1
            return
        end if
        ! - calculate coeff
        if (ijob == SID_COEFF + 14) then
            call rci_op_null(task)
            TrHddSdd = resvec(1)
            coeff(0) = 2.0_r8*TrH - TrHS
            coeff(1) = 2.0_r8*(2.0_r8*TrHd - TrHdS - TrHSd)
            coeff(2) = 2.0_r8*(TrHdd - TrHdSd - TrHdSdT) - TrHddS - TrHSdd
            coeff(3) = -2.0_r8*(TrHddSd + TrHdSdd)
            coeff(4) = -TrHddSdd
            coeff(2) = 2.0_r8*coeff(2)
            coeff(3) = 3.0_r8*coeff(3)
            coeff(4) = 4.0_r8*coeff(4)
            e_min = coeff(0)
            if (icg > 0) then
                ijob = SID_UPDATE
            else
                ijob = SID_ITER
            end if
            return
        end if

        ! this is the main loop of the CG algorithm. We perform a series
        ! of line minimizations, with the gradient G at each new step being
        ! modified to obtain the search direction D

        ! - D = PG
        if (ijob == SID_ITER) then
            call rci_op_copy(iS, task, 'N', MID_PG, MID_D)
            ijob = ijob + 1
            return
        end if

        ! - D = PG + lambda PGp
        if (ijob == SID_ITER + 1) then
            call rci_op_axpy(iS, task, m, n, lambda, &
                             MID_PGp, m, MID_D, m)
            ijob = ijob + 1
            return
        end if
        ! - G_p = G
        if (ijob == SID_ITER + 2) then
            call rci_op_copy(iS, task, 'N', MID_G, MID_Gp)
            ijob = ijob + 1
            return
        end if
        ! - PG_p = PG
        if (ijob == SID_ITER + 3) then
            call rci_op_copy(iS, task, 'N', MID_PG, MID_PGp)
            ijob = ijob + 1
            return
        end if
        ! - e_min_old = e_min
        if (ijob == SID_ITER + 4) then
            call rci_op_null(task)
            e_min_old = e_min
            if (icg <= 0) then
                ijob = SID_UPDATE
            else
                ijob = ijob + 1
            end if
            return
        end if

        ! if this is not the first CG step, we have to recalculate HWd,
        ! SWd, HWdd, SWdd, and the coeffs.
        ! if icg > 0
        ! HWd = D'*HC
        if (ijob <= SID_ITER + 5) then
            call rci_op_gemm(iS, task, 'C', 'N', n, n, m, 1.0_r8, &
                             MID_D, m, MID_HC, m, 0.0_r8, MID_HWd, max_n)
            ijob = ijob + 1
            return
        end if
        ! SWd = D'*SC
        if (ijob == SID_ITER + 6) then
            if (ovlp_is_unit) then
                call rci_op_gemm(iS, task, 'C', 'N', n, n, m, 1.0_r8, &
                                 MID_D, m, MID_C, m, 0.0_r8, MID_SWd, max_n)
            else
                call rci_op_gemm(iS, task, 'C', 'N', n, n, m, 1.0_r8, &
                                 MID_D, m, MID_SC, m, 0.0_r8, MID_SWd, max_n)
            end if
            ijob = ijob + 1
            return
        end if

        ! - calculate HWdd = D'*H*D = D'*(H*D)
        ! -- HD = H*D
        if (ijob == SID_ITER + 7) then
            call rci_op_h_multi(iS, task, 'N', m, n, MID_D, MID_HD)
            ijob = ijob + 1
            return
        end if

        ! -- SD = S*D
        if (ijob == SID_ITER + 8) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_s_multi(iS, task, 'N', m, n, MID_D, MID_SD)
            end if
            ijob = ijob + 1
            return
        end if

        if (ijob == SID_ITER + 9) then
            if (ovlp_is_unit) then
                call rci_op_axpy(iS, task, m, n, -est_ub, MID_D, m, MID_HD, m)
            else
                call rci_op_axpy(iS, task, m, n, -est_ub, MID_SD, m, MID_HD, m)
            end if
            ijob = ijob + 1
            return
        end if

        ! -- HWdd = D'*HD
        if (ijob == SID_ITER + 10) then
            call rci_op_gemm(iS, task, 'C', 'N', n, n, m, 1.0_r8, &
                             MID_D, m, MID_HD, m, 0.0_r8, &
                             MID_HWdd, max_n)
            ijob = ijob + 1
            return
        end if

        !  SWdd=D'*S*D
        ! - calculate SWdd = D'*S*D
        ! -- SWdd = D'*SD
        if (ijob == SID_ITER + 11) then
            if (ovlp_is_unit) then
                call rci_op_gemm(iS, task, 'C', 'N', n, n, m, 1.0_r8, &
                                 MID_D, m, MID_D, m, 0.0_r8, &
                                 MID_SWdd, max_n)
            else
                call rci_op_gemm(iS, task, 'C', 'N', n, n, m, 1.0_r8, &
                                 MID_D, m, MID_SD, m, 0.0_r8, &
                                 MID_SWdd, max_n)
            end if
            ijob = SID_COEFF
            return
        end if

        ! using the coeffs. calculated anlytically, we can find
        ! the minimum of the functional in the search direction,
        ! and calculate the energy at that minimum
        if (ijob == SID_UPDATE) then
            call rci_op_null(task)
            call rci_omm_solve_cubic(coeff(1:4), x_min)
            ! if the line search is successful, move to the minimum
            e_min = coeff(4)*x_min**4/4.0_r8 + &
                    coeff(3)*x_min**3/3.0_r8 + &
                    coeff(2)*x_min**2/2.0_r8 + &
                    coeff(1)*x_min + &
                    coeff(0)
            icg = icg + 1
            e_diff = 2.0_r8*abs((e_min - e_min_old) / &
                (e_min + e_min_old + est_ub*n*2))
            resvec(1) = e_min
            if (r_h%verbose > 0) write(*,'(a,i5,a,es15.7e3)') &
                'Iter ', icg, ', Rel Err: ', e_diff
            if (e_diff <= tol_iter) then
                conv = .true.
                ijob = SID_FINISH
                call rci_op_null(task)
                return
            end if
            if (icg > max_iter) then
                ijob = SID_FINISH
                call rci_op_null(task)
                return
            end if
            ijob = SID_UPDATE + 1
            return
        end if

        ! - C_min = x_min*D + C_min
        if (ijob == SID_UPDATE + 1) then
            call rci_op_axpy(iS, task, m, n, x_min, MID_D, m, MID_C, m)
            ijob = ijob + 1
            return
        end if

        ! - HWdT = HWd'
        if (ijob == SID_UPDATE + 2) then
            call rci_op_copy(iS, task, 'C', MID_HWd, MID_HWdT)
            ijob = ijob + 1
            return
        end if

        ! - HW = x_min*HWdT + HW
        if (ijob == SID_UPDATE + 3) then
            call rci_op_axpy(iS, task, n, n, x_min, MID_HWdT, max_n, &
                             MID_HW, max_n)
            ijob = ijob + 1
            return
        end if

        ! - HW = x_min*HWd + HW
        if (ijob == SID_UPDATE + 4) then
            call rci_op_axpy(iS, task, n, n, x_min, MID_HWd, max_n, &
                             MID_HW, max_n)
            ijob = ijob + 1
            return
        end if

        ! - HW = x_min^2*HWdd + HW
        if (ijob == SID_UPDATE + 5) then
            call rci_op_axpy(iS, task, n, n, x_min**2, &
                             MID_HWdd, max_n, MID_HW, max_n)
            ijob = ijob + 1
            return
        end if

        ! - SWdT = SWd'
        if (ijob == SID_UPDATE + 6) then
            call rci_op_copy(iS, task, 'C', MID_SWd, MID_SWdT)
            ijob = ijob + 1
            return
        end if

        ! - SW = x_min*SWdT + SW
        if (ijob == SID_UPDATE + 7) then
            call rci_op_axpy(iS, task, n, n, x_min, MID_SWdT, max_n, &
                             MID_SW, max_n)
            ijob = ijob + 1
            return
        end if

        ! - SW = x_min*SWd + SW
        if (ijob == SID_UPDATE + 8) then
            call rci_op_axpy(iS, task, n, n, x_min, MID_SWd, max_n, &
                             MID_SW, max_n)
            ijob = ijob + 1
            return
        end if

        ! - SW = x_min^2*SWdd + SW
        if (ijob == SID_UPDATE + 9) then
            call rci_op_axpy(iS, task, n, n, x_min**2, &
                             MID_SWdd, max_n, MID_SW, max_n)
            ijob = ijob + 1
            return
        end if

        ! recalculate G
        ! - HC = x_min*HD + HC
        if (ijob == SID_UPDATE + 10) then
            call rci_op_axpy(iS, task, m, n, x_min, MID_HD, m, MID_HC, m)
            ijob = ijob + 1
            return
        end if

        ! - SC = x_min*SD + SC
        if (ijob == SID_UPDATE + 11) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_axpy(iS, task, m, n, x_min, MID_SD, m, MID_SC, m)
            end if
            ijob = ijob + 1
            return
        end if

        ! - G = -2 HC*SW
        if (ijob == SID_UPDATE + 12) then
            call rci_op_gemm(iS, task, 'N', 'N', m, n, n, -2.0_r8, &
                             MID_HC, m, MID_SW, max_n, 0.0_r8, MID_G, m)
            ijob = ijob + 1
            return
        end if
        ! - G = -2 SC*HW + G
        if (ijob == SID_UPDATE + 13) then
            if (ovlp_is_unit) then
                call rci_op_gemm(iS, task, 'N', 'N', m, n, n, -2.0_r8, &
                                 MID_C, m, MID_HW, max_n, 1.0_r8, MID_G, m)
            else
                call rci_op_gemm(iS, task, 'N', 'N', m, n, n, -2.0_r8, &
                                 MID_SC, m, MID_HW, max_n, 1.0_r8, MID_G, m)
            end if
            ijob = ijob + 1
            return
        end if
        ! - G = 4*HC + G
        if (ijob == SID_UPDATE + 14) then
            call rci_op_axpy(iS, task, m, n, 4.0_r8, MID_HC, m, MID_G, m)
            ijob = ijob + 1
            return
        end if

        ! - PG = P*G
        if (ijob == SID_UPDATE + 15) then
            call rci_op_p_multi(iS, task, 'N', m, n, MID_G, MID_PG)
            ijob = SID_LS_CONV
            return
        end if

        ! calculate lambda
        if (ijob == SID_LS_CONV) then
            call rci_op_null(task)
            if (ls_conv) then
                ijob = ijob + 1
                return
            else
                lambda = 0.0_r8
                ijob = SID_ITER
                return
            end if
        end if
        ! Work = G
        if (ijob == SID_LS_CONV + 1) then
            call rci_op_copy(iS, task, 'N', MID_G, MID_WORK)
            ijob = ijob + 1
            return
        end if
        ! Work = - Gp + Work
        if (ijob == SID_LS_CONV + 2) then
            call rci_op_axpy(iS, task, m, n, -1.0_r8, &
                             MID_Gp, m, MID_WORK, m)
            ijob = ijob + 1
            return
        end if
        ! lambda_n = trace(PG*Work)
        if (ijob == SID_LS_CONV + 3) then
            call rci_op_dot(iS, task, m, n, MID_PG, m, MID_WORK, m)
            ijob = ijob + 1
            return
        end if
        ! lambda_d = trace(PGp*Gp)
        if (ijob == SID_LS_CONV + 4) then
            lambda_n = resvec(1)
            call rci_op_dot(iS, task, m, n, MID_PGp, m, MID_Gp, m)
            ijob = ijob + 1
            return
        end if
        ! lambda = lambda_n/lambda_d
        if (ijob == SID_LS_CONV + 5) then
            lambda_d = resvec(1)
            lambda = lambda_n/lambda_d
            call rci_op_null(task)
            ijob = SID_ITER
            return
        end if

        ! Determine whether to do RR
        if (ijob == SID_FINISH) then
            r_h%total_iter = icg
            ijob = ijob + 1
            call rci_op_null(task)
            return
        end if

        ! - HW = C'*HC
        if (ijob == SID_FINISH + 1) then
            call rci_op_gemm(iS, task, 'C', 'N', n, n, &
                             m, 1.0_r8, &
                             MID_C, m, MID_HC, m, 0.0_r8, &
                             MID_HW, max_n)
            ijob = ijob + 1
            return
        end if

        ! - SW = C'*SC
        if (ijob == SID_FINISH + 2) then
            if (ovlp_is_unit) then
                call rci_op_gemm(iS, task, 'C', 'N', n, n, &
                                 m, 1.0_r8, &
                                 MID_C, m, MID_C, m, 0.0_r8, &
                                 MID_SW, max_n)
            else
                call rci_op_gemm(iS, task, 'C', 'N', n, n, &
                                 m, 1.0_r8, &
                                 MID_C, m, MID_SC, m, 0.0_r8, &
                                 MID_SW, max_n)
            end if
            ijob = ijob + 1
            return
        end if

        ! - solve a RR eigenvalue problem
        ! -- HW * X = SW * X * Diag(EW) eigenvector is in HW
        if (ijob == SID_FINISH + 3) then
            call rci_op_hegv(iS, task, 'V', 'L', n, &
                         MID_HW, max_n, MID_SW, max_n)
            ijob = ijob + 1
            return
        end if

        ! - Psi = Psi*G
        if (ijob == SID_FINISH + 4) then
            resvec(1:n) = resvec(1:n) + est_ub
            r_h%total_energy = sum(resvec(1:n))
            if (r_h%verbose > 0) then
                write(*,*) 'Eigenvalues are:'
                print *, resvec(1:n)
            end if
            call rci_op_gemm(iS, task, 'N', 'N', m, n, &
                             n, 1.0_r8, &
                             MID_C, m, MID_HW, max_n, 0.0_r8, &
                             MID_WORK, m)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_FINISH + 5) then
            call rci_op_copy(iS, task, 'N', MID_WORK, MID_C)
            ijob = ijob + 1
            return
        end if

        if (ijob == SID_FINISH + 6) then
            call rci_op_converge(task, conv)
            return
        end if

    end subroutine

end module ELSI_RCI_OMM
