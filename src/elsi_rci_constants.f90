! Copyright (c) 2015-2019, the ELSI team.
! All rights reserved.
!
! This file is part of ELSI and is distributed under the BSD 3-clause license,
! which may be found in the LICENSE file in the ELSI root directory.

!>
!! This module contains constants used in ELSI RCI.
!!
module ELSI_RCI_CONSTANTS

    use, intrinsic :: ISO_C_BINDING
    use ELSI_RCI_PRECISION, only:i4

    implicit none

    !&<
    ! Constant of solver
    integer(i4), parameter :: RCI_SOLVER            = -1
    integer(i4), parameter :: RCI_SOLVER_DAVIDSON   =  1
    integer(i4), parameter :: RCI_SOLVER_OMM        =  2
    integer(i4), parameter :: RCI_SOLVER_PPCG       =  3
    integer(i4), parameter :: RCI_SOLVER_CHEBFILTER =  4

    ! Constant of ijob
    integer(i4), parameter :: RCI_INIT_IJOB  = -1

    ! Constant of task
    integer(i4), parameter :: RCI_NULL       =  0
    integer(i4), parameter :: RCI_STOP       =  1
    integer(i4), parameter :: RCI_CONVERGE   =  2
    integer(i4), parameter :: RCI_ALLOCATE   =  3
    integer(i4), parameter :: RCI_DEALLOCATE =  4
    integer(i4), parameter :: RCI_H_MULTI    =  5
    integer(i4), parameter :: RCI_S_MULTI    =  6
    integer(i4), parameter :: RCI_P_MULTI    =  7
    integer(i4), parameter :: RCI_COPY       =  8
    integer(i4), parameter :: RCI_SUBCOPY    =  9
    integer(i4), parameter :: RCI_SUBCOL     = 10
    integer(i4), parameter :: RCI_SUBROW     = 11
    integer(i4), parameter :: RCI_TRACE      = 12
    integer(i4), parameter :: RCI_DOT        = 13
    integer(i4), parameter :: RCI_SCALE      = 14
    integer(i4), parameter :: RCI_COLSCALE   = 15
    integer(i4), parameter :: RCI_ROWSCALE   = 16
    integer(i4), parameter :: RCI_AXPY       = 17
    integer(i4), parameter :: RCI_COL_NORM   = 18
    integer(i4), parameter :: RCI_GEMM       = 19
    integer(i4), parameter :: RCI_HEEV       = 20
    integer(i4), parameter :: RCI_HEGV       = 21
    integer(i4), parameter :: RCI_POTRF      = 22
    integer(i4), parameter :: RCI_TRSM       = 23
    !&>

end module ELSI_RCI_CONSTANTS
