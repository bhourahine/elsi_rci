/* Copyright (c) 2015-2021, the ELSI team.
   All rights reserved.

   This file is part of ELSI and is distributed under the BSD 3-clause license,
   which may be found in the LICENSE file in the ELSI root directory. */

#ifndef ELSI_RCI_H_INCLUDED
#define ELSI_RCI_H_INCLUDED

#define DECLARE_HANDLE(name) struct name##__ { int unused; }; \
                             typedef struct name##__ *name

struct rci_handle__ {
         int n_basis;
         int n_state;
         int max_n;
         int ovlp_is_unit;
         int solver;
         int max_iter;
         int n_res;
         double tol_iter;
         int verbose;

         int total_iter;
         int total_energy;

         double omm_est_ub;

         int ppcg_sbsize;
         int ppcg_rrstep;
         double ppcg_tol_lock;

         double cheb_est_lb;
         double cheb_est_ub;
         double cheb_max_inneriter;};
typedef struct rci_handle__ *rci_handle;

struct rci_instr__ {
        char jobz, uplo; 
        char side; 
        char trH, trS, trP, trA, trB;

        int m, n;
        int k;
        int lda, ldb, ldc;

        int rAoff,cAoff; 
        int rBoff,cBoff;

        int Aidx, Bidx, Cidx;

        double alpha, beta;
};
typedef struct rci_instr__ *rci_instr;

#ifdef __cplusplus
extern "C"{
#endif

void c_rci_init(rci_handle *r_h_c, 
                rci_instr *iS_c,
                int solver,
                int n_basis,
                int n_state,
                double tol_iter,
                int max_iter,
                int verbose);

void c_rci_solve_allocate(rci_handle r_h_c,
                          int *ijob,
                          rci_instr iS_c,
                          int *task);

void c_rci_solve_deallocate(rci_handle r_h_c,
                            int *ijob,
                            rci_instr iS_c,
                            int *task);

void c_rci_solve(rci_handle handle_c,
                 int *ijob,
                 rci_instr iS_c,
                 int *task,
                 double *resvec_c);

extern int rci_solver_c            ;
extern int rci_solver_davidson_c   ;
extern int rci_solver_omm_c        ;
extern int rci_solver_ppcg_c       ;       
extern int rci_solver_chebfilter_c ;
extern int rci_init_ijob_c         ;
extern int rci_null_c              ;
extern int rci_stop_c              ;
extern int rci_converge_c          ;
extern int rci_allocate_c          ;
extern int rci_deallocate_c        ;
extern int rci_h_multi_c           ;
extern int rci_s_multi_c           ;
extern int rci_p_multi_c           ;
extern int rci_copy_c              ;
extern int rci_subcopy_c           ;
extern int rci_subcol_c            ;
extern int rci_subrow_c            ;
extern int rci_trace_c             ;
extern int rci_dot_c               ;
extern int rci_scale_c             ;
extern int rci_colscale_c          ;
extern int rci_rowscale_c          ;
extern int rci_axpy_c              ;
extern int rci_col_norm_c          ;
extern int rci_gemm_c              ;
extern int rci_heev_c              ;
extern int rci_hegv_c              ;
extern int rci_potrf_c             ;
extern int rci_trsm_c              ;

#ifdef __cplusplus
}
#endif

#endif

