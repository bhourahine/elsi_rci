### Generic Intel ###

SET(CMAKE_Fortran_COMPILER "ifort" CACHE STRING "Fortran compiler")
SET(CMAKE_Fortran_FLAGS "-g -heap-arrays -O3 -xAVX -fp-model precise" CACHE STRING "Fortran flags")

SET(LIB_PATHS "$ENV{MKLROOT}/lib/intel64" CACHE STRING "External library paths")
SET(LIBS "mkl_intel_lp64 mkl_sequential mkl_core" CACHE STRING "External libraries")
