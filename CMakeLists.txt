### CMake version ###
CMAKE_MINIMUM_REQUIRED(VERSION 3.0 FATAL_ERROR)

### Project ###
PROJECT(elsi_rci VERSION 0.1.0 LANGUAGES Fortran)

### CMake modules ###
LIST(APPEND CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake)
INCLUDE(color_message)
INCLUDE(external_libraries)

### Installation paths ###
IF(PROJECT_BINARY_DIR STREQUAL PROJECT_SOURCE_DIR)
  MESSAGE(FATAL_ERROR "${MAGENTA}Build in the source directory is not allowed${COLORRESET}")
ENDIF()
SET(CMAKE_Fortran_MODULE_DIRECTORY "${PROJECT_BINARY_DIR}/include")
SET(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}/lib")
SET(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}/lib")
SET(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}/bin")
IF(NOT DEFINED CMAKE_INSTALL_INCLUDEDIR)
  SET(CMAKE_INSTALL_INCLUDEDIR "include" CACHE PATH "Location to install module and header files")
ENDIF()
IF(NOT DEFINED CMAKE_INSTALL_LIBDIR)
  SET(CMAKE_INSTALL_LIBDIR "lib" CACHE PATH "Location to install library files")
ENDIF()

### Options ###
OPTION(ENABLE_TESTS "Build ELSI_RCI test programs" ON)
OPTION(BUILD_SHARED_LIBS "Whether to build shared or static libraries" OFF)
SET(LIB_PATHS "" CACHE STRING "List of directories containing libraries to be linked against")
SET(INC_PATHS "" CACHE STRING "List of include directories")
SET(LIBS "" CACHE STRING "List of external libraries to be linked against")

### External libraries ###
STRING(REPLACE "-l" "" LIBS "${LIBS}")
FOREACH(str LIBS LIB_PATHS INC_PATHS)
  convert_to_list(${str})
ENDFOREACH()
generate_library_targets(LIB_PATHS LIBS elsi_rci)

### Compilations ###
INCLUDE_DIRECTORIES(${CMAKE_Fortran_MODULE_DIRECTORY})

ADD_SUBDIRECTORY(src)

IF(ENABLE_TESTS)
  MESSAGE(STATUS "Enabling test programs")
  ENABLE_TESTING()
  ADD_SUBDIRECTORY(test)
ENDIF()

#CONFIGURE_FILE(${PROJECT_SOURCE_DIR}/.elsi_rci.pc.in
#  ${CMAKE_ARCHIVE_OUTPUT_DIRECTORY}/pkgconfig/elsi_rci.pc
#  @ONLY)
#
#INSTALL(TARGETS EXPORT elsi_rciConfig INCLUDES DESTINATION
#  ${CMAKE_INSTALL_INCLUDEDIR})
#
#EXPORT(EXPORT elsi_rciConfig NAMESPACE elsi_rci:: FILE
#  elsi_rciConfig.cmake)
